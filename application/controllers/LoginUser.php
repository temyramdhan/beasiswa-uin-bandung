<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginUser extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->library('user_agent');
		if ($this->agent->is_robot()){
			redirect('https://www.google.com/');
		}
		if (!$this->session->userdata('beasiswa_logged')<>1) {
            redirect('User');
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper('beasiswa_helper');
    }
	public function index()
	{
		$data = array(
			"alur_daftar" => $this->Beasiswa_Model->alurDaftar()
		);
		$this->load->view('template_login_user', $data);
	}
	public function Auth()
	{
		$login_true = "<?xml version='1.0'?><data><response>TRUE</response></data>";
		$nim = $this->input->post('nim');
		$password = $this->input->post('password');
		
		if(Login_SALAM($nim, $password) == $login_true)
		{
			$data = array(
				"beasiswa_logged" => TRUE,
				"beasiswa_nim" => $nim,
				"beasiswa_bag" => 'Peserta'
			);
			$this->session->set_userdata($data);
			redirect('User');
		}else{
			$this->session->set_flashdata('notif', Notif('error','NIM atau Password tidak sesuai', 'Cek kembali NIM dan password kamu.'));
			redirect(base_url());
		}
	}
	/*
	public function Test($nim)
	{
		$arrContextOptions = array(
		"ssl" => array(
			"verify_peer" => false,
			"verify_peer_name" => false,
		)
		);
		$no = 0;
		$service = file_get_contents("http://api.alfi-gusman.web.id/salam/Api/listSemester/?nim=".$nim."&token=b97c190ce853064efe15ff431565dcf1", false, stream_context_create($arrContextOptions));
		$serviceEncode = json_encode($service, TRUE);
		$serviceDecode = json_decode($service, TRUE);
		
		echo '<pre>';
		print_r($serviceDecode);
		echo '</pre>';
		echo count($serviceDecode['data']);
	}
	*/
}

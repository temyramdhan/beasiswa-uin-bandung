<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Datapeserta extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		if (!$this->session->userdata('beasiswa_logged')<>1) {
            redirect('User');
        }
		if ($this->session->userdata('beasiswa_adm_logged')<>1) {
            redirect('LoginAdmin');
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper(array('adm_beasiswa_helper'));
		$this->load->library(array('datatables'));
    }
	public function index()
	{
		$data = array(
			"title" => "Data Peserta",
			//"nim" => $this->session->userdata('beasiswa_adm_username'),
			"filter_jk" => array('L','P'),
			"filter_status" => array('LULUS','TIDAK LULUS', 'PENDING'),
			"provinsi" => $this->Beasiswa_Model->Provinsi(),
			"kota" => $this->Beasiswa_Model->Kota(),
			"fakultas" => $this->Beasiswa_Model->Fakultas(),
			"jurusan" => $this->Beasiswa_Model->Jurusan(),
			"alur_daftar" => $this->Beasiswa_Model->alurDaftar(),
			"att" => array('target' => '_blank')
		);
		$this->template->load('template_admin', 'admin/datapeserta', $data);
	}
	public function detail($id)
	{
		$nim = filterReport('biodata_mahasiswa', 'id_biodata', $id, 'NIM');
		$alur = filterReport('biodata_mahasiswa', 'id_biodata', $id, 'alur_daftar');
		$periode = filterReport('biodata_mahasiswa', 'id_biodata', $id, 'periode');
		
		$data = array(
			"title" => "Detail Peserta",
			'url' => 'datapeserta/ubah',
			"id" => $id,
			"nim" => $nim,
			'periode' => $periode,
			"provinsi" => $this->Beasiswa_Model->Provinsi(),
			"kota" => $this->Beasiswa_Model->Kota(),
			"agama" => json_decode($this->jsonAgama()),
			"pekerjaan" => json_decode($this->jsonPekerjaan()),
			"jk" => json_decode($this->jsonJK()),
			"goldar" => json_decode($this->jsonGoldar()),
			"status" => json_decode($this->jsonStatus()),
			"alur_daftar" => $this->Beasiswa_Model->alurDaftarWhere($alur),
			"kode_alur_daftar" => $alur,
			"detail" => $this->Beasiswa_Model->getDetailPeserta($id)->result()
		);
		$this->template->load('template_admin', 'admin/detailpeserta', $data);
	}
	public function jsonDatapeserta()
	{
		header('Content-Type: application/json');
		$this->datatables->select('id_biodata, NIM, UPPER(nama_mhs) AS nama, tgl_daftar, jurusan, provinsi, kota, alur_daftar');
		$this->datatables->from('biodata_mahasiswa');
		//$this->datatables->where('id_batch_fk !=', '0');
		$this->datatables->add_column('aksi', '
			<a href="datapeserta/detail/$1" target="_blank" class="btn btn-link btn-secondary btn-lg text-center" style="font-size:20px"> <i class="fas fa-file-alt"></i> </a>
		', 'id_biodata');
		echo $this->datatables->generate();
	}
	public function filter_report()
	{
		// POST Variable
		$stat = $this->input->post('status');
		$jk = $this->input->post('jk');
		$fakultas = $this->input->post('fakultas');
		$jurusan = $this->input->post('jurusan');
		$provinsi = $this->input->post('provinsi');
		if($this->input->post('kota') != "" || $this->input->post('kota') != NULL)
		{
			$kota = $this->input->post('kota');
		}
		else
		{
			$kota = 0;
		}
		$alur_daftar = $this->input->post('alur_daftar');
		$filetype = $this->input->post('filetype');
		
		// Execute SQL
		$this->db->select('*');
		$this->db->from('biodata_mahasiswa');
		if($stat != "" || $stat != NULL)
		{
			$this->db->where('status_lulus', $stat);
		}
		$this->db->like('jeniskelamin', $jk, 'both');
		$this->db->like('fakultas', $fakultas, 'both');
		$this->db->like('jurusan', $jurusan, 'both');
		$this->db->like('provinsi', $provinsi, 'both');
		$this->db->like('kota', $kota, 'both');
		$this->db->like('alur_daftar', $alur_daftar, 'both');
		$query = $this->db->get();
		
		// Result
		$data = array(
			"no" => 1,
			"result" => $query->result(),
			"row" => $query->num_rows(),
			"jk" => $jk,
			"fakultas" => $fakultas,
			"jurusan" => $jurusan,
			"provinsi" => $provinsi,
			"kota" => $kota,
			"alur_daftar" => $alur_daftar,
			"stat" => $stat
		);
		
		if($filetype == "pdf")
		{
			$this->load->view('admin/laporan_data_peserta', $data);	
		}
		else if($filetype == "cover")
		{
			$this->load->view('admin/cover_data_peserta', $data);	
		}
		else
		{
			$this->session->set_flashdata('notif', Notif('info','Coming Soon', 'Coming Soon.'));
			redirect('datapeserta');
		}
	}
	
	// JSON
	public function jsonAgama()
	{
		$data = array('Islam','Kristen','Katholik','Budha','Konghucu','Lainnya');
		return json_encode($data);
	}
	public function jsonPekerjaan()
	{
		$data = array('Petani','PNS','Peg. Swasta','TNI/POLRI','Pensiunan','Purnawirawan/Veteran','Pedagang','Lainnya');
		return json_encode($data);
	}
	public function jsonJK()
	{
		$data = array('L','P');
		return json_encode($data);
	}
	public function jsonGoldar()
	{
		$data = array('A','B','O','AB');
		return json_encode($data);
	}
	public function jsonStatus()
	{
		$data = array('Hidup','Meninggal');
		return json_encode($data);
	}
}
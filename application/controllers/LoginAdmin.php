<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginAdmin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		$this->load->library('user_agent');
		if ($this->agent->is_robot()){
			redirect('https://www.google.com/');
		}
		if (!$this->session->userdata('beasiswa_logged')<>1) {
            redirect('Admin');
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper('beasiswa_helper');
    }
	public function index()
	{
		$data = array(
			"alur_daftar" => $this->Beasiswa_Model->alurDaftar()
		);
		$this->load->view('template_login_admin', $data);
	}
	public function Auth()
	{
		$username = md5($this->input->post('username'));
		$password = md5($this->input->post('password'));
		
		$this->db->select('*');
		$this->db->from('master');
		$this->db->where('username', $username);
		$this->db->where('password', $password);
		$this->db->where('level', 'Admin');
		$this->db->where('status', 1);
		$log = $this->db->get();
		
		if($log->num_rows() != 0)
		{
			foreach($log->result() as $Log)
			{
				$data = array(
					"beasiswa_adm_logged" => TRUE,
					"beasiswa_adm_name" => $Log->name,
					"beasiswa_adm_bag" => 'Admin'
				);
				$this->session->set_userdata($data);
				redirect('Admin');
			}
			//echo "Sukses";
		}else{
			$this->session->set_flashdata('notif', Notif('error','Username atau Password tidak sesuai', 'Cek kembali Username dan password kamu.'));
			redirect('LoginAdmin');
		}
	}
}

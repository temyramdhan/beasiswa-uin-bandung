<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelolamenu extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		if (!$this->session->userdata('beasiswa_logged')<>1) {
            redirect('User');
        }
		if ($this->session->userdata('beasiswa_adm_logged')<>1) {
            redirect('LoginAdmin');
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper(array('beasiswa_helper','grafik_helper'));
		$this->load->library(array('datatables'));
    }
	public function index()
	{
		$data = array(
			"title" => "Kelola Menu",
			"nim" => $this->session->userdata('beasiswa_adm_username'),
			//"alur_daftar" => $this->Beasiswa_Model->alurDaftar()
		);
		$this->template->load('template_admin', 'admin/kelolamenu', $data);
	}
	public function jsonKelolamenu()
	{
		header('Content-Type: application/json');
		$this->datatables->select('id_menu, nama_menu, sub_menu, link_menu, pengelola_menu, status_menu');
		$this->datatables->from('tbl_menu');
		echo $this->datatables->generate();
	}
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		if (!$this->session->userdata('beasiswa_logged')<>1) {
            redirect('User');
        }
		if ($this->session->userdata('beasiswa_adm_logged')<>1) {
            redirect('LoginAdmin');
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper(array('beasiswa_helper','grafik_helper'));
		//date_default_timezone_set('Asia/Jakarta');
    }
	public function index()
	{
		$data = array(
			"title" => "Dashboard",
			"nim" => $this->session->userdata('beasiswa_adm_username'),
			"alur_daftar" => $this->Beasiswa_Model->alurDaftar()
		);
		$this->template->load('template_admin', 'admin/dashboard', $data);
	}
	public function Tes()
	{
		$a = date('Y-m-d H:i:s');
		echo $a."<br/>";
		echo strtotime('now')."<br/>";
		$b = strtotime($a);
		echo date('Y-m-d H:i:s', $b);
		if($b < '1588694958')
		{
			echo " A";
		}else{
			echo " B";
		}
		//echo Data_SALAM_1('1137050211');
		//echo $this->session->userdata('beasiswa_adm_username');
	}
	public function logout()
	{
		$this->session->unset_userdata('beasiswa_adm_logged');
		$this->session->unset_userdata('beasiswa_adm_username');
		$this->session->unset_userdata('beasiswa_adm_bag');
		redirect('LoginAdmin');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pendaftaran_beasiswa_ppa extends CI_Controller {

	private $alurDaftarBea = "PPA";
	private $judulAlurDaftarBea = "Peningkatan Prestasi Akademik";
	private $urlAlurDaftarBea = "pendaftaran_beasiswa_ppa";
	private $viewAlurDaftarBea = "pendaftaran_beasiswa";
	
	function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('beasiswa_logged')<>1) {
            redirect(base_url());
        }
		//if (!$this->session->userdata('beasiswa_logged')) { }
		$this->load->model('Beasiswa_Model');
		$this->load->helper('beasiswa_helper');
		$this->load->library('upload');
	}
	public function index()
	{
		redirect($this->urlAlurDaftarBea.'/step/1');
	}
	public function step($step)
	{
		// Step 3
		$labelSeputar = array(
			'Skill/Kemampuan apa yang dimiliki',
			'Potensi diri apa yang dimiliki',
			'Aktivitas Sosial Akademis/Non Akademis',
			'Saran Untuk Pengembangan Beasiswa PPA'
		);
		$idSeputar = array(
			'skill',
			'potensi',
			'aktivitas',
			'saran'
		);
		
		// Final Step
		$labelLampiran = array(
			'Biodata Mahasiswa (.pdf)',
			'Foto Profil Mahasiswa (.jpg)',
			'Scan KTP (.jpg)',
			'Scan KTM (.jpg)',
			'Transkrip Nilai Sampai Semester Akhir (.pdf)',
			'Motivation Letter (.pdf)',
			'Surat Aktif Dari GENBI (.pdf)',
			'Surat Rekomendasi Tokoh (.pdf)',
		);
		$idLampiran = array(
			'lamp_bio',
			'lamp_foto',
			'lamp_ktp',
			'lamp_ktm',
			'lamp_transkrip',
			'lamp_ml',
			'lamp_genbi',
			'lamp_srt',
		);
		
		// Stat Pendaftaran
		$tglAwal = strtotime(filterReport('alur_daftar', 'id', $this->alurDaftarBea, 'tgl_pendaftaran_mulai'));
		$tglAkhir = strtotime(filterReport('alur_daftar', 'id', $this->alurDaftarBea, 'tgl_pendaftaran_berakhir'));
		$ipk_beasiswa = filterReport('alur_daftar', 'id', $this->alurDaftarBea, 'min_ipk');
		$ipk_mhs = Data_SALAM($this->session->userdata('beasiswa_nim'), 'mhsIpkTranskrip');
		$prodi = $this->Beasiswa_Model->getProdiAlurDaftar($this->alurDaftarBea, Data_SALAM($this->session->userdata('beasiswa_nim'), 'mhsProdiKode'))->num_rows();
		
		$data = array(
			"url" => $this->urlAlurDaftarBea,
			"title" => "Pendaftaran Beasiswa ".$this->alurDaftarBea." (".$this->judulAlurDaftarBea.")",
			"nim" => $this->session->userdata('beasiswa_nim'),
			"provinsi" => $this->Beasiswa_Model->Provinsi(),
			"kota" => $this->Beasiswa_Model->Kota(),
			"agama" => json_decode($this->jsonAgama()),
			"pekerjaan" => json_decode($this->jsonPekerjaan()),
			"jk" => json_decode($this->jsonJK()),
			"goldar" => json_decode($this->jsonGoldar()),
			"status" => json_decode($this->jsonStatus()),
			"alur_daftar" => $this->Beasiswa_Model->alurDaftarWhere($this->alurDaftarBea),
			"kode_alur_daftar" => $this->alurDaftarBea,
			"label_lampiran" => $labelLampiran,
			"id_lampiran" => $idLampiran,
			"label_seputar" => $labelSeputar,
			"id_seputar" => $idSeputar,
			"step" => $step,
			"statPendaftaran" => statPendaftaran($this->session->userdata('beasiswa_nim'), $tglAwal, $tglAkhir, 1, 8, $ipk_mhs, $ipk_beasiswa, $prodi)
		);
		$this->template->load('template', 'user/'.$this->viewAlurDaftarBea, $data);
	}
	public function tahap_1($alur_daftar)
	{
		// Inialisasi Variabel
		$getNim = $this->input->post('nim');
		$getPeriode = date('Y');
		$getAlurDaftar = $alur_daftar;
		$getRows = $this->Beasiswa_Model->getBeasiswa($getNim, $getAlurDaftar, $getPeriode)->num_rows();
		
		// Notifikasi
		$icon = "success";
		$title = "Step 1 Berhasil Tersimpan";
		$text = "Step 1 Biodata Mahasiswa sudah berhasil tersimpan, anda tinggal melengkapi step berikutnya.";
		
		// Save
		$data = array(
			// Diperlukan
			"NIM" => $this->input->post('nim'),
			"nama_mhs" => $this->input->post('nama'),
			"fakultas" => Prodi_SALAM($getNim, 'id_fakultas'),
			"jurusan" => Prodi_SALAM($getNim, 'id_jurusan'),
			"IPK" => Data_SALAM($getNim, 'mhsIpkTranskrip'),
			// Data bisa disimpan/diubah
			"jeniskelamin" => $this->input->post('jk'),
			"email" => $this->input->post('email'),
			"no_telepon" => $this->input->post('notelp'),
			"tgl_lahir" => $this->input->post('tgllahir'),
			"kodepos" => $this->input->post('kodepos'),
			"provinsi" => $this->input->post('provinsi'),
			"kota" => $this->input->post('kota'),
			"provinsi_lahir" => $this->input->post('provinsi'),
			"tempat_lahir" => $this->input->post('kota'),
			"nik_ktp" => $this->input->post('nik_ktp'),
			"nama_ktp" => $this->input->post('nama_ktp'),
			"goldar_ktp" => $this->input->post('goldar_ktp'),
			"agama_ktp" => $this->input->post('agama_ktp'),
			"alamat_ktp" => $this->input->post('alamat_ktp'),
			"alamat" => $this->input->post('alamat'),
			// Diperlukan
			"periode" => $getPeriode,
			"alur_daftar" => $getAlurDaftar,
			"tgl_daftar" => date('Y-m-d H:i:s')
		);
		
		// Cek biodata
		if($getRows == 0)
		{
			$this->Save($data);
		}
		else
		{
			$this->Update($getNim, $getAlurDaftar, $getPeriode, $data);
		}
		
		// Redirect
		$this->session->set_flashdata("notif", Notif($icon, $title, $text));
		redirect($this->urlAlurDaftarBea."/step/2");
	}
	public function tahap_2($alur_daftar)
	{
		// Inialisasi Variabel
		$getNim = $this->session->userdata('beasiswa_nim');
		$getPeriode = date('Y');
		$getAlurDaftar = $alur_daftar;
		$getRows = $this->Beasiswa_Model->getBeasiswa($getNim, $getAlurDaftar, $getPeriode)->num_rows();
		
		// Notifikasi
		$icon = "success";
		$title = "Step 2 Berhasil Tersimpan";
		$text = "Step 2 Data Keluarga sudah berhasil tersimpan, anda tinggal melengkapi step berikutnya.";
		
		$data = array(
			// Diperlukan
			"NIM" => $getNim,
			"nama_mhs" => Data_SALAM($getNim, 'mhsNama'),
			"fakultas" => Prodi_SALAM($getNim, 'id_fakultas'),
			"jurusan" => Prodi_SALAM($getNim, 'id_jurusan'),
			"IPK" => Data_SALAM($getNim, 'mhsIpkTranskrip'),
			// Data bisa disimpan/diubah
			"nama_ayah" => strtoupper($this->input->post('nama_ayah')),
			"nama_ibu" => strtoupper($this->input->post('nama_ibu')),
			"provinsi_ortu" => $this->input->post('provinsi_ortu'),
			"kota_ortu" => $this->input->post('kota_ortu'),
			"status_ayah" => $this->input->post('status_ayah'),
			"status_ibu" => $this->input->post('status_ibu'),
			"no_telepon_ortu" => $this->input->post('no_telp_ortu'),
			"pekerjaan_orangtua" => $this->input->post('pekerjaan'),
			"alamat_ortu" => $this->input->post('alamat'),
			// Diperlukan
			"periode" => $getPeriode,
			"alur_daftar" => $getAlurDaftar,
			"tgl_daftar" => date('Y-m-d H:i:s')
		);
		
		// Cek biodata
		if($getRows == 0)
		{
			$this->Save($data);
		}
		else
		{
			$this->Update($getNim, $getAlurDaftar, $getPeriode, $data);
		}
		
		// Redirect
		$this->session->set_flashdata("notif", Notif($icon, $title, $text));
		redirect($this->urlAlurDaftarBea."/step/3");
	}
	public function tahap_3($alur_daftar)
	{
		// Inialisasi Variabel
		$getNim = $this->session->userdata('beasiswa_nim');
		$getPeriode = date('Y');
		$getAlurDaftar = $alur_daftar;
		$getRows = $this->Beasiswa_Model->getBeasiswa($getNim, $getAlurDaftar, $getPeriode)->num_rows();
		
		// Notifikasi
		$icon = "success";
		$title = "Step 3 Berhasil Tersimpan";
		$text = "Step 3 Pertanyaan Seputar Beasiswa sudah berhasil tersimpan, anda tinggal melengkapi step berikutnya.";
		
		// Data Seputar Beasiswa
		$seputar[] = array(
			"skill" => $this->input->post('skill'),
			"potensi" => $this->input->post('potensi'),
			"aktivitas" => $this->input->post('aktivitas'),
			"saran" => $this->input->post('saran'),
		);
		
		$data = array(
			// Diperlukan
			"NIM" => $getNim,
			"nama_mhs" => Data_SALAM($getNim, 'mhsNama'),
			"fakultas" => Prodi_SALAM($getNim, 'id_fakultas'),
			"jurusan" => Prodi_SALAM($getNim, 'id_jurusan'),
			"IPK" => Data_SALAM($getNim, 'mhsIpkTranskrip'),
			// Data bisa disimpan/diubah
			"seputar_beasiswa" => json_encode($seputar, TRUE),
			// Diperlukan
			"periode" => $getPeriode,
			"alur_daftar" => $getAlurDaftar,
			"tgl_daftar" => date('Y-m-d H:i:s')
		);
		
		// Cek biodata
		if($getRows == 0)
		{
			$this->Save($data);
		}
		else
		{
			$this->Update($getNim, $getAlurDaftar, $getPeriode, $data);
		}
		
		// Redirect
		$this->session->set_flashdata("notif", Notif($icon, $title, $text));
		redirect($this->urlAlurDaftarBea."/step/final");
	}
	public function final($alur_daftar)
	{
		// Inialisasi Variabel
		$getNim = $this->session->userdata('beasiswa_nim');
		$getPeriode = date('Y');
		$getAlurDaftar = $alur_daftar;
		$getRows = $this->Beasiswa_Model->getBeasiswa($getNim, $getAlurDaftar, $getPeriode)->num_rows();
		
		// Inialisasi Lampiran
		$lBio 		= UploadLampiran('lamp_bio', 'file_bio', 'pdf', $getNim, $getPeriode, 'biodata');
		$lFoto 		= UploadLampiran('lamp_foto', 'file_foto', 'jpg|jpeg', $getNim, $getPeriode, 'mahasiswa');
		$lKtp 		= UploadLampiran('lamp_ktp', 'file_ktp', 'jpg|jpeg', $getNim, $getPeriode, 'ktp');
		$lKtm 		= UploadLampiran('lamp_ktm', 'file_ktm', 'jpg|jpeg', $getNim, $getPeriode, 'ktm');
		$lTranskrip = UploadLampiran('lamp_transkrip', 'file_transkrip', 'pdf', $getNim, $getPeriode, 'transkrip');
		$lMl 		= UploadLampiran('lamp_ml', 'file_ml', 'pdf', $getNim, $getPeriode, 'motivation_letter');
		$lGenbi 	= UploadLampiran('lamp_genbi', 'file_genbi', 'pdf', $getNim, $getPeriode, 'genbi');
		$lSrt 		= UploadLampiran('lamp_srt', 'file_srt', 'pdf', $getNim, $getPeriode, 'srt');
		
		if($lBio == "0" || $lFoto == "0" || $lKtp == "0" || $lKtm == "0" || $lTranskrip == "0" || $lMl == "0" || $lGenbi == "0" || $lSrt == "0")
		{
			$icon = "error";
			$title = "Final Step Gagal Tersimpan";
			$text = "Final step gagal tersimpan. Untuk lebih lengkapnya silahkan lihat status pada Final Step";
			
			$this->session->set_flashdata("notif", Notif($icon, $title, $text));
			redirect($this->urlAlurDaftarBea."/step/final");
		}
		// Data Lampiran
		$lampiran[] = array(
			"lamp_bio" => $lBio,
			"lamp_foto" => $lFoto,
			"lamp_ktp" => $lKtp,
			"lamp_ktm" => $lKtm,
			"lamp_transkrip" => $lTranskrip,
			"lamp_ml" => $lMl,
			"lamp_genbi" => $lGenbi,
			"lamp_srt" => $lSrt
		);
		
		$data = array(
			// Diperlukan
			"NIM" => $getNim,
			"nama_mhs" => Data_SALAM($getNim, 'mhsNama'),
			"fakultas" => Prodi_SALAM($getNim, 'id_fakultas'),
			"jurusan" => Prodi_SALAM($getNim, 'id_jurusan'),
			"IPK" => Data_SALAM($getNim, 'mhsIpkTranskrip'),
			// Data bisa disimpan/diubah
			"lampiran" => json_encode($lampiran, TRUE),
			// Diperlukan
			"periode" => $getPeriode,
			"alur_daftar" => $getAlurDaftar,
			"tgl_daftar" => date('Y-m-d H:i:s')
		);
		
		// Cek biodata
		if($getRows == 0)
		{
			$this->Save($data);
		}
		else
		{
			$this->Update($getNim, $getAlurDaftar, $getPeriode, $data);
		}
		
		// Cek lampiran apabula bernilai 0
		$statBio 	= getLampiran($getNim, $getAlurDaftar, 'lamp_bio');
		$statFoto	= getLampiran($getNim, $getAlurDaftar, 'lamp_foto');
		$statKtp 	= getLampiran($getNim, $getAlurDaftar, 'lamp_ktp');
		$statKtm 	= getLampiran($getNim, $getAlurDaftar, 'lamp_ktm');
		$statTrans 	= getLampiran($getNim, $getAlurDaftar, 'lamp_transkrip');
		$statMl 	= getLampiran($getNim, $getAlurDaftar, 'lamp_ml');
		$statGenbi 	= getLampiran($getNim, $getAlurDaftar, 'lamp_genbi');
		$statSrt 	= getLampiran($getNim, $getAlurDaftar, 'lamp_srt');
		
		/*
		if($statBio == "0" || $statFoto == "0" || $statKtp == "0" || $statKtm == "0" || $statTrans == "0" || $statMl == "0" || $statGenbi == "0" || $statSrt == "0")
		{
			$icon = "error";
			$title = "Final Step Gagal Tersimpan";
			$text = "Final step gagal tersimpan. Untuk lebih lengkapnya silahkan lihat status pada Final Step";
		}
		else
		{
		*/
			$icon = "success";
			$title = "Final Step Berhasil Tersimpan";
			$text = "Final Step Berkas Lampiran sudah berhasil tersimpan, Silahkan cek kembali pada final step apabila ada upload yang gagal. Anda tinggal menunggu konfirmasi dari admin/staff.";
			
		//}
		
		// Redirect
		$this->session->set_flashdata("notif", Notif($icon, $title, $text));
		redirect($this->urlAlurDaftarBea."/step/final");
	}
	
	// Action Save & Update
	public function Save($data)
	{
		$this->db->insert('biodata_mahasiswa', $data);
	}
	public function Update($nim, $alur_daftar, $periode, $data)
	{
		$this->db->where('NIM', $nim);
		$this->db->where('alur_daftar', $alur_daftar);
		$this->db->where('periode', $periode);
		$this->db->update('biodata_mahasiswa', $data);
	}
	
	// JSON
	public function jsonAgama()
	{
		$data = array('Islam','Kristen','Katholik','Budha','Konghucu','Lainnya');
		return json_encode($data);
	}
	public function jsonPekerjaan()
	{
		$data = array('Petani','PNS','Peg. Swasta','TNI/POLRI','Pensiunan','Purnawirawan/Veteran','Pedagang','Lainnya');
		return json_encode($data);
	}
	public function jsonJK()
	{
		$data = array('L','P');
		return json_encode($data);
	}
	public function jsonGoldar()
	{
		$data = array('A','B','O','AB');
		return json_encode($data);
	}
	public function jsonStatus()
	{
		$data = array('Hidup','Meninggal');
		return json_encode($data);
	}
}
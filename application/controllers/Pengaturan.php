<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		if (!$this->session->userdata('beasiswa_logged')<>1) {
            redirect('User');
        }
		if ($this->session->userdata('beasiswa_adm_logged')<>1) {
            redirect('LoginAdmin');
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper(array('adm_beasiswa_helper', 'opsi_helper'));
		$this->load->library(array('datatables'));
    }
	public function index()
	{
		$data = array(
			"title" => "Pengaturan",
			//"nim" => $this->session->userdata('beasiswa_adm_username'),
			"alur_daftar" => $this->Beasiswa_Model->alurDaftar(),
			"fak" => $this->Beasiswa_Model->Fakultas(),
			"jur" => $this->Beasiswa_Model->Jurusan(),
			"att" => array('target' => '_blank')
		);
		$this->template->load('template_admin', 'admin/pengaturan', $data);
	}
	public function update_prodi_alur_daftar($id)
	{
		$checkProdiAlurDaftar = $this->Beasiswa_Model->getProdiAlurDaftar($this->input->post('prodi_alur_daftar'), $this->input->post('prodi'))->num_rows();
		$data = array(
			"prodi" => $this->input->post('prodi'),
			"alur_daftar" => $this->input->post('prodi_alur_daftar'),
			"tgl" => date('Y-m-d H:i:s')
		);
		
		if($checkProdiAlurDaftar == 0)
		{
			$this->db->where('id', $id);
			$this->db->update('prodi_alur_daftar', $data);
		
			$title = "Prodi Alur Daftar Telah Diubah";
			$text = "Program studi untuk alur daftar ".$this->input->post('prodi_alur_daftar')." berhasil diubah.";
			$state = "secondary";
		}else{
			$title = "Prodi Alur Daftar Sudah Tersedia";
			$text = "Program studi untuk alur daftar ".$this->input->post('prodi_alur_daftar')." sudah tersedia sebelumnya.";
			$state = "info";
		}
		
		$this->session->set_flashdata('notif', Notif_2('fas fa-check', $state, 'center', 'top', $title, $text));
		redirect('Pengaturan');
	}
	public function hapus_prodi_alur_daftar($id)
	{
		$data = array(
			"id" => $id
		);
		
		$this->db->delete('prodi_alur_daftar', $data);
		
		$title = "Prodi Alur Daftar Telah Dihapus";
		$text = "Program studi untuk alur daftar ".$this->input->post('prodi_alur_daftar')." berhasil dihapus.";
		
		$this->session->set_flashdata('notif', Notif_2('fas fa-check', 'secondary', 'center', 'top', $title, $text));
		redirect('Pengaturan');
	}
	public function tambah_prodi_alur_daftar()
	{
		$checkProdiAlurDaftar = $this->Beasiswa_Model->getProdiAlurDaftar($this->input->post('prodi_alur_daftar'), $this->input->post('prodi'))->num_rows();
		$data = array(
			"prodi" => $this->input->post('prodi'),
			"alur_daftar" => $this->input->post('prodi_alur_daftar'),
			"tgl" => date('Y-m-d H:i:s')
		);
		
		if($checkProdiAlurDaftar == 0)
		{
			$this->db->insert('prodi_alur_daftar', $data);
		
			$title = "Prodi Alur Daftar Telah Ditambah";
			$text = "Program studi untuk alur daftar ".$this->input->post('prodi_alur_daftar')." berhasil ditambah.";
			$state = "secondary";
		}else{
			$title = "Prodi Alur Daftar Sudah Tersedia";
			$text = "Program studi untuk alur daftar ".$this->input->post('prodi_alur_daftar')." sudah tersedia sebelumnya.";
			$state = "info";
		}
		
		$this->session->set_flashdata('notif', Notif_2('fas fa-check', $state, 'center', 'top', $title, $text));
		redirect('Pengaturan');
	}
	public function update_alur_daftar()
	{
		$id = $this->input->post('id');
		$ipk = $this->input->post('ipk');
		$link = $this->input->post('link');
		$nama_alur_daftar = $this->input->post('nama_alur_daftar');
		$logo = UploadLogo('logo', $id, 'png');
		$tgl_pendaftaran_mulai = $this->input->post('tgl_pendaftaran_mulai')." ".$this->input->post('wkt_pendaftaran_mulai');
		$tgl_pendaftaran_berakhir = $this->input->post('tgl_pendaftaran_berakhir')." ".$this->input->post('wkt_pendaftaran_berakhir');
		$tgl_pengumpulan_mulai = $this->input->post('tgl_pengumpulan_mulai')." ".$this->input->post('wkt_pengumpulan_mulai');
		$tgl_pengumpulan_berakhir = $this->input->post('tgl_pengumpulan_berakhir')." ".$this->input->post('wkt_pengumpulan_berakhir');
		$tgl_pengumuman_kelulusan_mulai = $this->input->post('tgl_pengumuman_kelulusan_mulai')." ".$this->input->post('wkt_pengumuman_kelulusan_mulai');
		$tgl_pengumuman_kelulusan_berakhir = $this->input->post('tgl_pengumuman_kelulusan_berakhir')." ".$this->input->post('wkt_pengumuman_kelulusan_berakhir');
		$kriteria = $this->input->post('kriteria');
		$persyaratan = $this->input->post('persyaratan');
		$seleksi = $this->input->post('seleksi');
		$deskripsi = $this->input->post('deskripsi');
		
		$data = array(
			"kriteria" => $kriteria,
			"persyaratan" => $persyaratan,
			"deskripsi" => $deskripsi,
			"seleksi" => $seleksi,
			"nama_alur_daftar" => $nama_alur_daftar,
			"tgl_pendaftaran_mulai" => $tgl_pendaftaran_mulai,
			"tgl_pendaftaran_berakhir" => $tgl_pendaftaran_berakhir,
			"tgl_pengumpulan_mulai" => $tgl_pengumpulan_mulai,
			"tgl_pengumpulan_berakhir" => $tgl_pengumpulan_berakhir,
			"tgl_pengumuman_kelulusan_mulai" => $tgl_pengumuman_kelulusan_mulai,
			"tgl_pengumuman_kelulusan_berakhir" => $tgl_pengumuman_kelulusan_berakhir,
			"min_ipk" => $ipk,
			"link" => $link
		);
		
		$this->db->where('id', $id);
		$this->db->update('alur_daftar', $data);
		
		if($logo != "0")
		{
			$this->db->where('id', $id);
			$this->db->update('alur_daftar', array(
				"logo" => $logo
			));
		}
		
		$menu = array(
			"nama_menu" => $nama_alur_daftar
		);
		
		$this->db->where('link_menu', $link);
		$this->db->update('tbl_menu', $menu);
		
		$title = "Alur Daftar Berhasil Dilakukan";
		$text = "Alur daftar ".$id." berhasil dilakukan.";
		$state = "secondary";
		$this->session->set_flashdata('notif', Notif_2('fas fa-check', $state, 'center', 'top', $title, $text));
		redirect('Pengaturan');
	}
	public function tambah_alur_daftar()
	{
		$id = $this->input->post('id');
		$ipk = $this->input->post('ipk');
		$link = $this->input->post('link');
		$nama_alur_daftar = $this->input->post('nama_alur_daftar');
		$logo = UploadLogo('logo', $id, 'png');
		$tgl_pendaftaran_mulai = $this->input->post('tgl_pendaftaran_mulai')." ".$this->input->post('wkt_pendaftaran_mulai');
		$tgl_pendaftaran_berakhir = $this->input->post('tgl_pendaftaran_berakhir')." ".$this->input->post('wkt_pendaftaran_berakhir');
		$tgl_pengumpulan_mulai = $this->input->post('tgl_pengumpulan_mulai')." ".$this->input->post('wkt_pengumpulan_mulai');
		$tgl_pengumpulan_berakhir = $this->input->post('tgl_pengumpulan_berakhir')." ".$this->input->post('wkt_pengumpulan_berakhir');
		$tgl_pengumuman_kelulusan_mulai = $this->input->post('tgl_pengumuman_kelulusan_mulai')." ".$this->input->post('wkt_pengumuman_kelulusan_mulai');
		$tgl_pengumuman_kelulusan_berakhir = $this->input->post('tgl_pengumuman_kelulusan_berakhir')." ".$this->input->post('wkt_pengumuman_kelulusan_berakhir');
		$kriteria = $this->input->post('kriteria');
		$persyaratan = $this->input->post('persyaratan');
		$seleksi = $this->input->post('seleksi');
		$deskripsi = $this->input->post('deskripsi');
		
		if($logo != "0")
		{
			$this->db->insert('alur_daftar', array(
				"id" => $id,
				"logo" => $logo
			));
		}else{
			$title = "Upload Logo Gagal Dilakukan";
			$text = "Upload logo pada alur daftar ".$id." gagal dilakukan. Format logo diharuskan png.";
			$state = "warning";
			$this->session->set_flashdata('notif', Notif_2('fas fa-exclamation', $state, 'center', 'top', $title, $text));
			redirect('Pengaturan');
		}
		
		$data = array(
			"kriteria" => $kriteria,
			"persyaratan" => $persyaratan,
			"deskripsi" => $deskripsi,
			"seleksi" => $seleksi,
			"nama_alur_daftar" => $nama_alur_daftar,
			"tgl_pendaftaran_mulai" => $tgl_pendaftaran_mulai,
			"tgl_pendaftaran_berakhir" => $tgl_pendaftaran_berakhir,
			"tgl_pengumpulan_mulai" => $tgl_pengumpulan_mulai,
			"tgl_pengumpulan_berakhir" => $tgl_pengumpulan_berakhir,
			"tgl_pengumuman_kelulusan_mulai" => $tgl_pengumuman_kelulusan_mulai,
			"tgl_pengumuman_kelulusan_berakhir" => $tgl_pengumuman_kelulusan_berakhir,
			"min_ipk" => $ipk,
			"link" => $link
		);
		
		$this->db->where('id', $id);
		$this->db->update('alur_daftar', $data);
		
		$menu = array(
			"id_menu" => "MENU-".randomDigit(4),
			"nama_menu" => $nama_alur_daftar,
			"sub_menu" => "0",
			"link_menu" => $link,
			"icon_menu" => "far fa-edit",
			"status_menu" => "AKTIF",
			"pengelola_menu" => "Peserta"
		);
		
		$this->db->insert('tbl_menu', $menu);
		
		$title = "Alur Daftar Berhasil Ditambah";
		$text = "Alur daftar ".$id." berhasil ditambah. Tentukan prodi untuk alur daftarnya.";
		$state = "success";
		$this->session->set_flashdata('notif', Notif_2('fas fa-check', $state, 'center', 'top', $title, $text));
		redirect('Pengaturan');
	}
}
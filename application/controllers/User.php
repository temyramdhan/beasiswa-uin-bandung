<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		if ($this->session->userdata('beasiswa_logged')<>1) {
            redirect(base_url());
        }
		$this->load->model('Beasiswa_Model');
		$this->load->helper('beasiswa_helper');
		$this->load->helper('grafik_helper');
    }
	public function index()
	{
		$data = array(
			"title" => "Peserta",
			"nim" => $this->session->userdata('beasiswa_nim'),
			"alur_daftar" => $this->Beasiswa_Model->alurDaftar()
		);
		$this->template->load('template', 'user/dashboard', $data);
	}
	public function SALAM()
	{

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.alfi-gusman.web.id/salam/Api/ProfileMhs/?nim=1137050211&token=b97c190ce853064efe15ff431565dcf1",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
		));

		$response = curl_exec($curl);
	
		curl_close($curl);
		echo $response;
	}
	public function Tes()
	{
		//echo Data_SALAM_1('1137050211');
		echo $this->session->userdata('beasiswa_nim');
	}
	public function logout()
	{
		$this->session->unset_userdata('beasiswa_logged');
		$this->session->unset_userdata('beasiswa_nim');
		$this->session->unset_userdata('beasiswa_bag');
		redirect(base_url());
	}
}

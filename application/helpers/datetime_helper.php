<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function Change_Date_To_Y_m_d($date)
{
	$var = str_replace('/', '-', $date);
	return date("Y-m-d", strtotime($var));
}
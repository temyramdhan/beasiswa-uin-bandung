<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function opsiJson($slug, $title)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getOpsi($slug)->result();
	
	foreach($table as $T)
	{
		foreach(json_decode($T->value) as $jsonT)
		{
			return $jsonT[$title];
		}
	}
}
function cmbProdiAlurDaftar($alur_daftar, $prodi, $val_field, $field)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getProdiAlurDaftar($alur_daftar, $prodi)->result();
	
	foreach($table as $T)
	{
		if($T->$field == $val_field)
		{
			return "selected";
		}
	}
}
function ProdiAlurDaftar($alur_daftar)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getProdiAlurDaftar($alur_daftar);
	return $table;
}
function getAlurDaftar($alur_daftar)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->alurDaftarWhere($alur_daftar);
	return $table;
}
function UploadLogo($variable_form, $filename, $type)
{
	$uinsgd = & get_instance();
	$uinsgd->load->library('upload');
	$config['upload_path'] = './assets/img/alur_daftar'; 
	$config['overwrite'] = TRUE;
    $config['allowed_types'] = $type; 
    $config['max_size'] = '2048'; 
    $config['file_name'] = $filename; 
	$uinsgd->upload->initialize($config);
	
	if ($uinsgd->upload->do_upload($variable_form)){
		$arr_data = $uinsgd->upload->data();
		return $arr_data['file_name'];
	}else{
		return 0;
		//return $this->upload->display_errors();
	}
}
function randomDigit($digits)
{
	return str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
}
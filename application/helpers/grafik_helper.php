<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function randColorPart() 
{
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}
// Call this function
function randColor() 
{
    return randColorPart() . randColorPart() . randColorPart();
}
// Convert randColor() hex to rgba
function hexToRgba($color, $opacity = false) 
{
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}
function Label_Alur_Daftar($choose_field)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$db = $uinsgd->db->query("SELECT periode FROM biodata_mahasiswa GROUP BY periode");
	
	foreach($db->result() as $field)
	{
		$label[] = $field->$choose_field;
	}
	return json_encode($label);
}
function Data_Alur_Daftar($choose_field, $where)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$db = $uinsgd->db->query("SELECT alur_daftar, periode, CAST(COUNT(id_biodata) AS UNSIGNED) AS jmh_biodata_mhs FROM biodata_mahasiswa WHERE alur_daftar='".$where."' GROUP BY periode");
	
	foreach($db->result() as $field)
	{
		$data[] = $field->$choose_field;
	}
	return json_encode($data);
}
function Dataset_Alur_Daftar()
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$db = $uinsgd->db->query("SELECT alur_daftar, periode, COUNT(id_biodata) AS jmh_biodata_mhs FROM biodata_mahasiswa GROUP BY alur_daftar");
	//$db2 = $uinsgd->db->query("SELECT alur_daftar, periode, COUNT(id_biodata) AS jmh_biodata_mhs FROM biodata_mahasiswa GROUP BY alur_daftar, periode");
	
	foreach($db->result() as $field)
	{
		$dataset[] = array(
			"label" => $field->alur_daftar,
			"borderColor" => "#".randColor(),
			"pointBorderColor" => "#FFF",
			"pointBackgroundColor" => "#".randColor(),
			"pointBorderWidth" => 2,
			"pointHoverRadius" => 4,
			"pointHoverBorderWidth" => 1,
			"pointRadius" => 4,
			"backgroundColor" => "transparent",
			"fill" => true,
			"borderWidth" => 2,
			"data" => json_decode(str_replace('"','',Data_Alur_Daftar('jmh_biodata_mhs', $field->alur_daftar)))
		);
	}
	return json_encode($dataset);
}

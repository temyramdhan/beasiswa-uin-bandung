<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function getMainMenu($bagian)
{
	$cbt = get_instance();
	$cbt->db->select('*');
	$cbt->db->from('tbl_menu');
	$cbt->db->where('status_menu', 'AKTIF');
	$cbt->db->where('pengelola_menu', $bagian);
	return $cbt->db->get();
}
function getActiveMainMenu($link)
{
	$link_accessed = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	
	if($link_accessed == $link)
	{
		return "active";
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function getProfilCBT($field)
{
	$cbt = get_instance();
	$cbt->db->select('*');
	$cbt->db->from('tbl_profil');
	$cbt->db->where('id_profil', '1');
	$query = $cbt->db->get()->row_array();
	return $query[$field];
}
function strLength($str, $min, $max)
{
	$len = strlen($str);
	if($len > $max){
		$convert_str = substr($str, $min, $max);
		$result = $convert_str."...";
        return $result;
    }else{
		return $str;
	}
}
function statPendaftaran($nim, $tglAwal, $tglAkhir, $smtAwal, $smtAkhir, $ipk_mhs, $ipk_beasiswa, $prodi)
{
	// 0 - Pendaftaran Belum Dimulai
	// 1 - Pendaftaran Dimulai
	// 2 - Pendaftaran Berakhir
	// 3 - Semester Awal Tidak Sesuai
	// 4 - Semester Akhir Tidak Sesuai
	// 5 - IPK Tidak Mencukupi
	
	/*
	$arrContextOptions = array(
		"ssl" => array(
			"verify_peer" => false,
			"verify_peer_name" => false,
		)
	);
	$service = file_get_contents("http://api.alfi-gusman.web.id/salam/Api/listSemester/?nim=".$nim."&token=b97c190ce853064efe15ff431565dcf1", false, stream_context_create($arrContextOptions));
	$serviceDecode = json_decode($service, TRUE);
	$smt = count($serviceDecode['data']);
	*/
	$smt = 4;
	$strTgl = strtotime(date('Y-m-d H:i:s'));
	
	if($strTgl > $tglAwal)
	{
		if($strTgl < $tglAkhir)
		{
			if($prodi == 0)
			{
				// Prodi Tidak Sesuai
				return 6;
			}
			else
			{
				if($smt < $smtAwal)
				{
					// Semester Awal Tidak Sesuai
					return 3;
				}
				else
				{
					if($smt <= $smtAkhir)
					{
						if($ipk_mhs < $ipk_beasiswa)
						{
							// IPK Tidak Mencukupi
							return 5;
						}
						else
						{
							// Pendaftaran Dimulai
							return 1;
						}
					}
					else
					{
						// Semester Akhir Tidak Sesuai
						return 4;
					}
				}
			}
		}
		else
		{
			// Pendaftaran Berakhir
			return 2;
		}
	}
	else
	{
		// Pendaftaran Belum Dimulai
		return 0;
	}
}
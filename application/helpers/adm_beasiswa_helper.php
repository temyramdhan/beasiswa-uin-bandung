<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function txtBeasiswa($nim, $alur_daftar, $field, $periode) 
{
    $uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->NIM == $nim && $T->alur_daftar == $alur_daftar && $T->periode == $periode)
		{
			return $T->$field;
		}
		else
		{
			return NULL;
		}
	}
}
function cmbBeasiswa($nim, $alur_daftar, $field, $id, $periode) 
{
    $uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->$field == $id)
		{
			return "selected";
		}
	}
}
function Provinsi($nim, $alur_daftar, $id, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->provinsi == $id)
		{
			return "selected";
		}
	}
}
function ProvinsiOrtu($nim, $alur_daftar, $id, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->provinsi_ortu == $id)
		{
			return "selected";
		}
	}
}
function JK($nim, $alur_daftar, $id, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->jeniskelamin == $id)
		{
			return "checked";
		}
	}
}
function StatusHM($nim, $alur_daftar, $anggota, $id, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->$anggota == $id)
		{
			return "checked";
		}
	}
}
function Kota($nim, $alur_daftar, $id, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->kota == $id)
		{
			return "selected";
		}
	}
}
function KotaOrtu($nim, $alur_daftar, $id, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->kota_ortu == $id)
		{
			return "selected";
		}
	}
}
function Notif($icon, $title, $text = NULL)
{
	$content = "
		Swal.fire({ 
			title: '".$title."',
			html: '".$text."',
			icon: '".$icon."'
		});
	";
	return $content;
}
function Notif_2($icon, $type, $aHorizontal, $aVertical, $title, $text = NULL)
{
	$content = "
		$.notify({
			icon: '".$icon."',
			title: '".$title."',
			message: '".$text."',
		},{
			type: '".$type."',
			placement: {
				from: '".$aVertical."',
				align: '".$aHorizontal."'
			},
			time: 1000,
		});
	";
	return $content;
}
function getDataBea($nim, $alur_daftar, $json, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->NIM == $nim && $T->alur_daftar == $alur_daftar && $T->periode == $periode)
		{
			if($T->seputar_beasiswa == NULL || $T->seputar_beasiswa == "")
			{
				return NULL;
			}
			else
			{
				$decode = json_decode($T->seputar_beasiswa, TRUE);
				foreach($decode as $D)
				{
					return $D[$json];
				}
			}
		}
		else
		{
			return NULL;
		}
	}
}
function getLampiran($nim, $alur_daftar, $json, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->NIM == $nim && $T->alur_daftar == $alur_daftar && $T->periode == $periode)
		{
			if($T->lampiran == NULL || $T->lampiran == "")
			{
				return NULL;
			}
			else
			{
				$decode = json_decode($T->lampiran, TRUE);
				foreach($decode as $D)
				{
					return $D[$json];
				}
			}
		}
		else
		{
			return NULL;
		}
	}
}
function getFotoProfil($nim, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getFotoProfil($nim, $periode)->result();
	$rows = $uinsgd->Beasiswa_Model->getFotoProfil($nim, $periode)->num_rows();
	
	if($rows == 0)
	{
		return "profile.png";
	}
	else
	{	
	
	foreach($table as $T)
	{
		if($T->NIM == $nim && $T->periode == $periode)
		{
			if($T->lampiran == NULL || $T->lampiran == "")
			{
				return "profile.png";
			}
			else
			{
				$decode = json_decode($T->lampiran, TRUE);
				foreach($decode as $D)
				{
					return "lampiran/mahasiswa/".$D['lamp_foto'];
				}
			}
		}
		else
		{
			return "profile.png";
		}
	}
	
	}
}
function getStatLampiran($nim, $alur_daftar, $json, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->NIM == $nim && $T->alur_daftar == $alur_daftar && $T->periode == $periode)
		{
			if($T->lampiran == NULL || $T->lampiran == "")
			{
				return NULL;
			}
			else
			{
				$decode = json_decode($T->lampiran, TRUE);
				foreach($decode as $D)
				{
					if($D[$json] == "0")
					{
						$result = "<small class='form-text' style='color:red;'>Lampiran gagal diupload</small>";
					}
					else
					{
						$result = "<small class='form-text' style='color:green;'>Lampiran berhasil diupload</small>";
					}
					return $result;
				}
			}
		}
		else
		{
			return NULL;
		}
	}
}
function getPendidikan($nim, $alur_daftar, $json, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->NIM == $nim && $T->alur_daftar == $alur_daftar && $T->periode == $periode)
		{
			if($T->pendidikan == NULL || $T->pendidikan == "")
			{
				return NULL;
			}
			else
			{
				$decode = json_decode($T->pendidikan, TRUE);
				foreach($decode as $D)
				{
					return $D[$json];
				}
			}
		}
		else
		{
			return NULL;
		}
	}
}
function UploadLampiran($variable_form, $filename, $type, $nim, $periode, $source)
{
	$uinsgd = & get_instance();
	$config['upload_path'] = './assets/img/lampiran/'.$source; 
	$config['overwrite'] = TRUE;
    $config['allowed_types'] = $type; 
    $config['max_size'] = '2048'; 
    $config['file_name'] = $filename."_".$nim."_".$periode; 
	$uinsgd->upload->initialize($config);
	
	if ($uinsgd->upload->do_upload($variable_form)){
		$arr_data = $uinsgd->upload->data();
		return $arr_data['file_name'];
	}else{
		return 0;
		//return $this->upload->display_errors();
	}
}
function StatusVerifikasi($nim, $alur_daftar, $periode)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	$table = $uinsgd->Beasiswa_Model->getBeasiswa($nim, $alur_daftar, $periode)->result();
	
	foreach($table as $T)
	{
		if($T->status_verifikasi == 1)
		{
			return "<p style='color:green; padding:0; margin:0;'><i style='font-size:25px;' class='icon-check'></i></p><span>Sudah Terverifikasi</span>";
		}
		else
		{
			return "<p style='color:red; padding:0; margin:0;'><i style='font-size:25px;' class='icon-close'></i></p><span>Belum Terverifikasi</span>";
		}
	}
}
function filterReport($table, $where, $value, $field)
{
	$uinsgd = & get_instance();
	$uinsgd->db->select('*');
	$uinsgd->db->from($table);
	$uinsgd->db->where($where, $value);
	$query = $uinsgd->db->get()->row_array();
	return $query[$field];
}
<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// API
function API_Salam($nim)
{
	$arrContextOptions = array(
		"ssl" => array(
			"verify_peer" => false,
			"verify_peer_name" => false,
		)
    );
	$service = file_get_contents("https://simak.uinsgd.ac.id/webservices/integrated/system-uin/Mahasiswa?mhsNiu=".$nim, false, stream_context_create($arrContextOptions));
	$serviceDecode = json_decode($service, TRUE);
	return $serviceDecode;
}
function Login_SALAM($nim, $pass)
{
	$api_token = "c07faba589286a285292e8db1a058c99";	
	$arrContextOptions = array(
			"ssl" => array(
				"verify_peer" => false,
				"verify_peer_name" => false,
			)
    );
	$service = file_get_contents("https://simak.uinsgd.ac.id/portal/serviceres/services/service.php?username=".$nim."&password=".$pass."&api=".$api_token."", false, stream_context_create($arrContextOptions));
	return $service;
}

// Data
function Data_SALAM($nim, $field)
{	
	foreach(API_Salam($nim) as $SD)
	{
		return $SD[$field];
	}
}
function Data_SALAM_1($nim)
{
	$arrContextOptions = array(
		"ssl" => array(
			"verify_peer" => false,
			"verify_peer_name" => false,
		)
    );
	$service = file_get_contents("https://simak.uinsgd.ac.id/webservices/integrated/system-uin/Mahasiswa?mhsNiu=".$nim, false, stream_context_create($arrContextOptions));
	echo $service;
}
function Prodi_SALAM($nim, $field)
{
	$uinsgd = & get_instance();
    $uinsgd->load->database();
	$uinsgd->load->model('Beasiswa_Model');
	
	/*
	$arrContextOptions = array(
		"ssl" => array(
			"verify_peer" => false,
			"verify_peer_name" => false,
		)
    );
	$service = file_get_contents("https://simak.uinsgd.ac.id/webservices/integrated/system-uin/Mahasiswa?mhsNiu=".$nim, false, stream_context_create($arrContextOptions));
	$serviceDecode = json_decode($service, TRUE);
	*/
	
	foreach(API_Salam($nim) as $SD)
	{
		foreach($uinsgd->Beasiswa_Model->Prodi($SD['mhsProdiKode'])->result() as $P)
		{
			return $P->$field;
		}
	}
}
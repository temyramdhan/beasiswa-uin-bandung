<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beasiswa_Model extends CI_Model 
{

	public function alurDaftar()
	{
		$this->db->select('*');
        $this->db->from('alur_daftar');
		return $this->db->get();
	}
	public function Provinsi()
	{
		$this->db->select('*');
        $this->db->from('provinsi');
		$this->db->order_by('id', 'ASC');
		return $this->db->get();
	}
	public function Kota()
	{
		$this->db->select('*');
        $this->db->from('kota');
		$this->db->order_by('id_kota', 'ASC');
		return $this->db->get();
	}
	public function Fakultas()
	{
		$this->db->select('*');
        $this->db->from('fakultas');
		return $this->db->get();
	}
	public function Jurusan()
	{
		$this->db->select('*');
        $this->db->from('jurusan');
		return $this->db->get();
	}
	public function alurDaftarWhere($where)
	{
		$this->db->select('*');
        $this->db->from('alur_daftar');
		$this->db->where('id', $where);
		return $this->db->get();
	}
	public function Prodi($kode_prodi)
	{
		$this->db->select('*');
        $this->db->from('v_prodi');
		$this->db->where('id_jurusan', $kode_prodi);
		return $this->db->get();
	}
	public function getBeasiswa($nim, $alur_daftar, $periode)
	{
		$this->db->select('*');
        $this->db->from('biodata_mahasiswa');
		$this->db->where('NIM', $nim);
		$this->db->where('alur_daftar', $alur_daftar);
		$this->db->where('periode', $periode);
		return $this->db->get();
	}
	public function getFotoProfil($nim, $periode)
	{
		$this->db->select('*');
        $this->db->from('biodata_mahasiswa');
		$this->db->where('NIM', $nim);
		$this->db->where('periode', $periode);
		$this->db->order_by('tgl_daftar', 'DESC');
		$this->db->limit('1');
		return $this->db->get();
	}
	public function getDetailPeserta($id)
	{
		$this->db->select('*');
        $this->db->from('biodata_mahasiswa');
		$this->db->where('id_biodata', $id);
		return $this->db->get();
	}
	public function getProdiAlurDaftar($alur_daftar, $prodi = NULL)
	{
		$this->db->select('*');
        $this->db->from('prodi_alur_daftar');
		$this->db->where('alur_daftar', $alur_daftar);
		if($prodi != NULL)
		{
			$this->db->where('prodi', $prodi);
		}
		return $this->db->get();
	}
	
	// JSON
	public function getKelolaMenu()
	{
		$this->datatables->select('nama_menu');
		$this->datatables->from('tbl_menu');
		return $this->datatables->generate();
	}
	public function getOpsi($slug)
	{
		$this->db->select('*');
        $this->db->from('pengaturan');
		$this->db->where('slug', $slug);
		return $this->db->get();
	}
}

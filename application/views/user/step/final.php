										<div class="col-md-12" style="padding-top: 20px;">
											<h3>(Final Step)</h3>
											<b>Upload Berkas Lampiran</b>
											<i style="color: red;" class="pull-right">Maksimal ukuran file 2 MB</i>
											<hr/>
										</div>
										<div class="col-md-12">
											<?=form_open_multipart($url.'/final/'.$kode_alur_daftar);?>
										</div>
										<?php
										for($i = 0; $i < count($id_lampiran); $i++) {
										?>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for=""><?=$label_lampiran[$i];?></label>
												<input type="file" class="form-control-file" id="" name="<?=$id_lampiran[$i];?>" required>
												<?=getStatLampiran($nim, $kode_alur_daftar, $id_lampiran[$i]);?>
											</div>
										</div>
										<?php } ?>
										<!--
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Foto Profil Mahasiswa (.jpg)</label>
												<input type="file" class="form-control-file" id="" name="lamp_foto" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Scan KTP (.jpg)</label>
												<input type="file" class="form-control-file" id="" name="lamp_ktp" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Scan KTM (.jpg)</label>
												<input type="file" class="form-control-file" id="" name="lamp_ktm" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Transkrip Nilai Sampai Semester Akhir (.pdf)</label>
												<input type="file" class="form-control-file" id="" name="lamp_transkrip" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Motivation Letter (.pdf)</label>
												<input type="file" class="form-control-file" id="" name="lamp_ml" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Surat Aktif Dari GENBI (.pdf)</label>
												<input type="file" class="form-control-file" id="" name="lamp_genbi" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Surat Rekomendasi Tokoh (.pdf)</label>
												<input type="file" class="form-control-file" id="" name="lamp_srt" required>
											</div>
										</div>
										-->
										<div class="col-md-12">
											<button class="btn btn-success btn-border btn-block" type="submit">
												<span class="btn-label">
													<i class="fas fa-save"></i>
												</span>
												Simpan
											</button>
										</div>
										<div class="col-md-12">
											<?=form_close();?>
										</div>
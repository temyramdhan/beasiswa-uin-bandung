										<div class="col-md-12" style="padding-top: 20px;">
											<h3>(Step 3)</h3>
											<b>Pertanyaan Seputar Beasiswa</b>
											<hr/>
										</div>
										<div class="col-md-12">
											<form method="post" action="<?=base_url()."/".$url;?>/tahap_3/<?=$kode_alur_daftar;?>">
										</div>
										<?php for($i = 0; $i < count($id_seputar); $i++) { ?>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for=""><?=$label_seputar[$i];?></label>
												<textarea class="form-control" id="" placeholder="Skill/Kemampuan" name="<?=$id_seputar[$i];?>" rows="5" required><?=getDataBea($nim, $kode_alur_daftar, $id_seputar[$i]);?></textarea>
											</div>
										</div>
										<?php } ?>
										<!--
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Potensi diri apa yang dimiliki</label>
												<textarea class="form-control" id="" placeholder="Potensi diri" name="potensi" rows="5" required><?=getDataBea($nim, $kode_alur_daftar, 'potensi');?></textarea>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Aktivitas Sosial Akademis/Non Akademis </label>
												<textarea class="form-control" id="" placeholder="Aktivitas Sosial" name="aktivitas" rows="5" required><?=getDataBea($nim, $kode_alur_daftar, 'aktivitas');?></textarea>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Saran Untuk Pengembangan Beasiswa Bank Indonesia </label>
												<textarea class="form-control" id="" placeholder="Saran Untuk Pengembangan Komunitas Beasiswa Bank Indonesia" name="saran" rows="5" required><?=getDataBea($nim, $kode_alur_daftar, 'saran');?></textarea>
											</div>
										</div>
										-->
										<div class="col-md-12">
											<button class="btn btn-success btn-border btn-block" type="submit">
												<span class="btn-label">
													<i class="fas fa-save"></i>
												</span>
												Simpan
											</button>
										</div>
										<div class="col-md-12">
											</form>
										</div>
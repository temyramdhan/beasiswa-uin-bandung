										<div class="col-md-12">
											<h3>(Step 1)</h3>
											<b>Biodata Mahasiswa</b>
											<hr/>
										</div>
										<div class="col-md-12">
											<form method="post" action="<?=base_url()."/".$url;?>/tahap_1/<?=$kode_alur_daftar;?>">
										</div>
										<div class="col-md-4">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">NIM</label>
												<input value="<?=Data_SALAM($nim, 'mhsNiu');?>" type="text" class="form-control" id="" placeholder="NIM" name="nim" readonly required>
											</div>
										</div>
										<div class="col-md-8">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Nama</label>
												<input value="<?=Data_SALAM($nim, 'mhsNama');?>" type="text" class="form-control" id="" placeholder="Nama" name="nama" readonly required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Fakultas</label>
												<input value="<?=Prodi_SALAM($nim, 'nama_fakultas');?>" type="text" class="form-control" id="" placeholder="Fakultas" name="fakultas" readonly required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Jurusan</label>
												<input value="<?=Prodi_SALAM($nim, 'nama_jurusan');?>" type="text" class="form-control" id="" placeholder="Jurusan" name="jurusan" readonly required>
											</div>
										</div>
										<div class="col-md-12">
											<?php 
											if(Data_SALAM($nim, 'mhsIpkTranskrip') < filterReport('alur_daftar', 'id', $kode_alur_daftar, 'min_ipk')) { 
												$text = "IPK tidak memenuhi syarat";
												$form = "has-error";
											} else {
												$text = "IPK telah memenuhi syarat";
												$form = "has-success";	
											} 
											?>
											<div class="form-group <?=$form;?>" style="padding-left:0; padding-right:0;">
												<label for="">IPK (Indeks Prestasi Kumulatif)</label>
												<input value="<?=Data_SALAM($nim, 'mhsIpkTranskrip');?>" type="text" class="form-control" id="" placeholder="IPK" name="ipk" readonly required>
												<small class="form-text text-muted"><?=$text;?></small>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Jenis Kelamin</label>
												<div class="selectgroup w-100">
													<?php for($i = 0; $i < count($jk); $i++) { ?>
													<label class="selectgroup-item">
														<input type="radio" name="jk" value="<?=$jk[$i];?>" class="selectgroup-input" <?=JK($nim, $kode_alur_daftar, $jk[$i]);?> required>
														<span class="selectgroup-button"><?=$jk[$i];?></span>
													</label>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Email</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'email');?>" type="email" class="form-control" id="" placeholder="Email" name="email" required>
												<small class="form-text text-muted">Pastikan email aktif.</small>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">No Telepon/HP/Whatsapp</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'no_telepon');?>" type="text" class="form-control" id="" placeholder="No Telepon/HP/Whatsapp" name="notelp" required>
												<small class="form-text text-muted">Pastikan nomor telepon bisa dihubungi.</small>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Tanggal Lahir</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'tgl_lahir');?>" type="date" data-date-format="YYYY-MM-DD" class="form-control" id="tglLahir" placeholder="Tanggal Lahir" name="tgllahir" required>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Kodepos</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'kodepos');?>" type="text" class="form-control" id="" placeholder="Kodepos" name="kodepos" required>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Provinsi</label>
												<select class="form-control" id="provinsi" placeholder="Provinsi" name="provinsi" required>
													<option value="">Pilih Provinsi</option>
													<?php foreach($provinsi->result() as $Prov) { ?>
													<option value="<?=$Prov->id;?>" <?=Provinsi($nim, $kode_alur_daftar, $Prov->id);?>><?=$Prov->nama;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Kota</label>
												<select class="form-control" id="kota" placeholder="Kota" name="kota" required>
													<option value="" data-chained="">Pilih Kota</option>
													<?php foreach($kota->result() as $Kota) { ?>
													<option value="<?=$Kota->id_kota;?>" data-chained="<?=$Kota->id_provinsi_fk;?>" <?=Kota($nim, $kode_alur_daftar, $Kota->id_kota);?>><?=$Kota->nama_kota;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">NIK KTP</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'nik_ktp');?>" type="text" class="form-control" id="" placeholder="NIK KTP" name="nik_ktp" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Nama (Sesuai KTP)</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'nama_ktp');?>" type="text" class="form-control" id="" placeholder="Nama KTP" name="nama_ktp" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Golongan Darah (Sesuai KTP)</label>
												<select class="form-control" id="goldar" placeholder="Golongan Darah" name="goldar_ktp" required>
													<option value="">Pilih Golongan Darah</option>
													<?php for($i = 0; $i < count($goldar); $i++) { ?>
													<option value="<?=$goldar[$i];?>" <?=cmbBeasiswa($nim, $kode_alur_daftar, 'goldar_ktp', $goldar[$i]);?>><?=$goldar[$i];?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Agama (Sesuai KTP)</label>
												<select class="form-control" id="agama" placeholder="Agama" name="agama_ktp" required>
													<option value="">Pilih Agama</option>
													<?php for($i = 0; $i < count($agama); $i++) { ?>
													<option value="<?=$agama[$i];?>" <?=cmbBeasiswa($nim, $kode_alur_daftar, 'agama_ktp', $agama[$i]);?>><?=$agama[$i];?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Alamat (Sesuai KTP)</label>
												<textarea class="form-control" id="" placeholder="Alamat KTP" name="alamat_ktp" rows="5" required><?=txtBeasiswa($nim, $kode_alur_daftar, 'alamat_ktp');?></textarea>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group" style="padding-left:0; padding-right:0;">
												<label for="">Alamat Sekarang</label>
												<textarea class="form-control" id="" placeholder="Alamat Sekarang" name="alamat" rows="5" required><?=txtBeasiswa($nim, $kode_alur_daftar, 'alamat');?></textarea>
											</div>
										</div>
										<div class="col-md-12">
											<button class="btn btn-success btn-border btn-block" id="tahap1" type="submit">
												<span class="btn-label">
													<i class="fas fa-save"></i>
												</span>
												Simpan
											</button>
										</div>
										<div class="col-md-12">
											</form>
										</div>
		<style>
		.alur-daftar { cursor: pointer; }
		.alur-daftar:hover { background-color: green; color: #ffffff; }
		.alur-daftar:active { background-color: green; color: #ffffff; }
		</style>
		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold">Dashboard</h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-12">
							<div class="card bg-success card-annoucement card-round">
								<div class="card-body text-center">
									<div class="card-opening text-white">Silahkan untuk memilih jalur masuk beasiswa.</div>
									<div class="card-desc text-white">
										Untuk kriteria, seleksi, dan persyaratan anda harus memilih jalur beasiswa terlebih dahulu.
									</div>
									<div class="row">
										<?php foreach($alur_daftar->result() as $AD) { ?>
										<div class="col-sm-6 col-lg-4">
											<div class="card p-3 alur-daftar" onclick="window.location = '<?=base_url().$AD->link;?>'">
												<div class="">
													<div class="row">
														<div class="col-md-12">
															<i style="font-size: 40px;" class="fas fa-user-graduate"></i>
														</div>
														<div class="col-md-12" align="center">
															<h5 class="fw-bold py-3" style="margin:0;"><?=$AD->nama_alur_daftar;?></h5>
														</div>
													</div>
												</div>
											</div>
										</div>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-8">
							<div class="card card-annoucement card-round">
								<div class="card-header">
									<div class="card-title">
										Jumlah Alur Beasiswa Peserta
									</div>
								</div>
								<div class="card-body text-center">
									<div class="chart-container">
										<canvas id="jmhAlurBeasiswa"></canvas>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card card-profile">
								<div class="card-header bg-success-gradient">
									<div class="profile-picture">
										<div class="avatar avatar-xl">
											<img src="<?=base_url();?>assets/img/<?=getFotoProfil($nim);?>" class="avatar-img rounded-circle">
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="user-profile text-center">
										<div class="name"><?=Data_SALAM($nim, 'mhsNama');?></div>
										<div class="job"><?=Prodi_SALAM($nim, 'nama_fakultas');?></div>
										<div class="desc"><?=Prodi_SALAM($nim, 'nama_jurusan');?></div>
										<div class="social-media">
											<a class="btn btn-info btn-twitter btn-sm btn-link" href="#"> 
												<span class="btn-label just-icon"><i class="flaticon-twitter"></i> </span>
											</a>
											<a class="btn btn-danger btn-sm btn-link" rel="publisher" href="#"> 
												<span class="btn-label just-icon"><i class="flaticon-google-plus"></i> </span> 
											</a>
											<a class="btn btn-primary btn-sm btn-link" rel="publisher" href="#"> 
												<span class="btn-label just-icon"><i class="flaticon-facebook"></i> </span> 
											</a>
											<a class="btn btn-danger btn-sm btn-link" rel="publisher" href="#"> 
												<span class="btn-label just-icon"><i class="flaticon-dribbble"></i> </span> 
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
		<script>
		function alurDaftar(link)
		{
			window.location = "<?=base_url();?>"+link;
		}
		var multipleLineChart = document.getElementById('jmhAlurBeasiswa').getContext('2d');
		
		var myMultipleLineChart = new Chart(multipleLineChart, {
			type: 'line',
			data: {
				labels: <?=Label_Alur_Daftar('periode');?>,
				datasets: <?=Dataset_Alur_Daftar();?>
			},
			options : {
				responsive: true, 
				maintainAspectRatio: false,
				legend: {
					position: 'top',
					labels : {
						padding: 10,
						fontColor: '#8a8a8a',
					}
				},
				tooltips: {
					bodySpacing: 4,
					mode:"nearest",
					intersect: 0,
					position:"nearest",
					xPadding:10,
					yPadding:10,
					caretPadding:10
				},
				layout:{
					padding: {
						left: 0,
						right: 0,
						top: 0,
						bottom: 0
					}
				}
			}
		});
		</script>
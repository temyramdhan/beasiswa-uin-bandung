		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold"><?=$title;?></h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<?php foreach($alur_daftar->result() as $AD) { ?>
						<div class="col-md-3">
							<div class="card card-annoucement card-round">
								<div class="card-body text-center">
									<div class="card-opening">
										<img class="mb-2" src="<?=base_url();?>assets/img/alur_daftar/<?=$AD->logo;?>" width="80" height="auto" />
									</div>
									<div class="card-desc">
										<p><?=$AD->deskripsi;?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<div class="card card-round">
								<div class="card-body">
									<div class="card-desc ">
										<p class="fw-bold">Persyaratan</p>
										<p><?=$AD->persyaratan;?></p>
										<p class="fw-bold">Kriteria</p>
										<p><?=$AD->kriteria;?></p>
										<p class="fw-bold">Seleksi Pendaftaran</p>
										<p><?=$AD->seleksi;?></p>
									</div>
								</div>
							</div>
						</div>
						<?php if($statPendaftaran == 1) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								Pastikan untuk input pendaftaran per step.
							</div>
						</div>
						<div class="col-md-12">
							<div class="card card-round">
								<div class="card-body">
									<div class="row">
										<?php $this->load->view('user/step/'.$step);?>
										<script>
										$('#kota').chained('#provinsi');
										$('#kota_ortu').chained('#provinsi_ortu');
										$("#tahap1").click(function(){
											Pace.restart();
										});
										<?=$this->session->flashdata("notif");?>
										</script>
									</div>
								</div>
							</div>
						</div>
						<?php } else if($statPendaftaran == 0) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								<center>
									<h1>Pendaftaran Belum Dimulai</h1>
								</center>
							</div>
						</div>
						<?php } else if($statPendaftaran == 2) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								Pendaftaran Sudah Berakhir
							</div>
						</div>
						<?php } else if($statPendaftaran == 3) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								Semester Awal Tidak Sesuai
							</div>
						</div>
						<?php } else if($statPendaftaran == 4) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								Semester Akhir Tidak Sesuai
							</div>
						</div>
						<?php } else if($statPendaftaran == 5) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								IPK Tidak Mencukupi
							</div>
						</div>
						<?php } else if($statPendaftaran == 6) { ?>
						<div class="col-md-12">
							<div class="alert alert-danger">
								Prodi Tidak Sesuai
							</div>
						</div>
						<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
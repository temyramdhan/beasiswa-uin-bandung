		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="<?=base_url();?>assets/img/profile.png" class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<small><?=$this->session->userdata('beasiswa_adm_name');?></small>
									<span class="user-level"><?=$this->session->userdata('beasiswa_adm_bag');?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="#profile">
											<span class="link-collapse">Profil Admin</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url();?>Pengaturan">
											<span class="link-collapse">Pengaturan</span>
										</a>
									</li>
									<li>
										<a href="<?=base_url();?>Admin/logout">
											<span class="link-collapse">Get Out</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-secondary">
						<li class="nav-item <?=getActiveMainMenu(base_url().'Admin');?>" id="">
							<a href="<?=base_url();?>Admin">
								<i class="fas fa-home"></i>
								<p>Dashboard</p>
							</a>
						</li>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section">Menu Utama</h4>
						</li>
						<?php foreach(getMainMenu($this->session->userdata("beasiswa_adm_bag"))->result() as $MainMenu) { ?>
						<li class="nav-item <?=getActiveMainMenu(base_url().$MainMenu->link_menu);?>" id="">
							<a href="<?=base_url().$MainMenu->link_menu;?>">
								<i class="<?=$MainMenu->icon_menu;?>"></i>
								<p><?=$MainMenu->nama_menu;?></p>
							</a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
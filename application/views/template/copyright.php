			<footer class="footer">
				<div class="container-fluid">
					<nav class="pull-left">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link" href="#">
									Author
								</a>
							</li>
						</ul>
					</nav>
					<div class="copyright ml-auto">
						Copyright &copy; Pusat Teknologi Informasi dan Pangkalan Data UIN Sunan Gunung Djati Bandung
					</div>				
				</div>
			</footer>
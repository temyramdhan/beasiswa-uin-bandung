		<div class="sidebar sidebar-style-2">			
			<div class="sidebar-wrapper scrollbar scrollbar-inner">
				<div class="sidebar-content">
					<div class="user">
						<div class="avatar-sm float-left mr-2">
							<img src="<?=base_url();?>assets/img/<?=getFotoProfil($nim);?>" class="avatar-img rounded-circle">
						</div>
						<div class="info">
							<a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
								<span>
									<small><?=Data_SALAM($nim, 'mhsNiu');?></small>
									<span class="user-level"><?=$this->session->userdata('beasiswa_bag');?></span>
									<span class="caret"></span>
								</span>
							</a>
							<div class="clearfix"></div>

							<div class="collapse in" id="collapseExample">
								<ul class="nav">
									<li>
										<a href="#profile">
											<span class="link-collapse">My Profile</span>
										</a>
									</li>
									<li>
										<a href="#edit">
											<span class="link-collapse">Edit Profile</span>
										</a>
									</li>
									<li>
										<a href="#settings">
											<span class="link-collapse">Settings</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<ul class="nav nav-success">
						<li class="nav-item <?=getActiveMainMenu(base_url().'User');?>" id="">
							<a href="<?=base_url();?>User">
								<i class="fas fa-home"></i>
								<p>Dashboard</p>
							</a>
						</li>
						<li class="nav-item" id="">
							<a href="<?=base_url();?>User/logout">
								<i class="fas fa-sign-out-alt"></i>
								<p>Logout</p>
							</a>
						</li>
						<?php
						if($this->session->userdata("beasiswa_bag") == "Peserta")
						{
							$section = "Alur Pendaftaran";
						}
						else
						{
							$section = "Menu Utama";
						}
						?>
						<li class="nav-section">
							<span class="sidebar-mini-icon">
								<i class="fa fa-ellipsis-h"></i>
							</span>
							<h4 class="text-section"><?=$section;?></h4>
						</li>
						<?php foreach(getMainMenu($this->session->userdata("beasiswa_bag"))->result() as $MainMenu) { ?>
						<li class="nav-item <?=getActiveMainMenu(base_url().$MainMenu->link_menu);?>" data-toggle="tooltip" data-placement="top" title="<?=$MainMenu->nama_menu;?>">
							<a href="<?=base_url().$MainMenu->link_menu;?>">
								<i class="<?=$MainMenu->icon_menu;?>"></i>
								<p><?=strLength($MainMenu->nama_menu, 0, 15);?></p>
							</a>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
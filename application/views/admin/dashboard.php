		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold">Dashboard</h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-12">
							<div class="card card-annoucement card-round">
								<div class="card-header">
									<div class="card-title">
										Jumlah Alur Beasiswa Peserta
									</div>
								</div>
								<div class="card-body text-center">
									<div class="chart-container">
										<canvas id="jmhAlurBeasiswa"></canvas>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
		<script>
		function alurDaftar(link)
		{
			window.location = "<?=base_url();?>"+link;
		}
		var multipleLineChart = document.getElementById('jmhAlurBeasiswa').getContext('2d');
		
		var myMultipleLineChart = new Chart(multipleLineChart, {
			type: 'line',
			data: {
				labels: <?=Label_Alur_Daftar('periode');?>,
				datasets: <?=Dataset_Alur_Daftar();?>
			},
			options : {
				responsive: true, 
				maintainAspectRatio: false,
				legend: {
					position: 'top',
					labels : {
						padding: 10,
						fontColor: '#8a8a8a',
					}
				},
				tooltips: {
					bodySpacing: 4,
					mode:"nearest",
					intersect: 0,
					position:"nearest",
					xPadding:10,
					yPadding:10,
					caretPadding:10
				},
				layout:{
					padding: {
						left: 0,
						right: 0,
						top: 0,
						bottom: 0
					}
				}
			}
		});
		</script>
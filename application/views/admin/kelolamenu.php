		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold"><?=$title;?></h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-12">
							<div class="card card-annoucement card-round">
								<div class="card-body">
									<div class="table-responsive">
										<table id="table" class="display table table-striped table-hover">
											<thead>
												<tr>
													<th>ID</th>
													<th>Title</th>
													<th>ID Sub Menu</th>
													<th>Link</th>
													<th>Pengelola</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<script>
						$('#table').DataTable({
							"pageLength": 5,
							"processing": true,
							"serverSide": true,
							"order": [],
							"ajax": {
								"url": "<?=base_url();?>Kelolamenu/jsonKelolamenu",
								"type": "POST"
							},
							"oLanguage": {
								sProcessing: "Tunggu Sebentar"
							},
							"columns": [
								{"aaData" : "id_menu"},
								{"aaData" : "nama_menu"},
								{"aaData" : "sub_menu"},
								{"aaData" : "link_menu"},
								{"aaData" : "pengelola_menu"},
								{"aaData" : "status_menu"}
							],
						});
						</script>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
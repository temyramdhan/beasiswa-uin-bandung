<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Laporan Data Peserta <?=getProfilCBT('title_profil');?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?=base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?=base_url();?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">

	
	<!--   Core JS Files   -->
	<script src="<?=base_url();?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/bootstrap.min.js"></script>
	
	<style>
	table {
		border-collapse: collapse;
	}
	table, th, td {
		padding: 4px;
		border: 1px solid black;
		font-size: 12px;
	}	
	.A4 {
		width: 210mm;
		height: 297mm;
	}
	.tnr {
		font-family : 'Times New Roman';
		padding:0;
		margin:0;
	}
	</style>

</head>
<body onload="window.print()">
	<div class="container">
		<div class="row">
			<!-- Header -->
			<div class="col-md-2 text-center">
				<img src="<?=base_url();?>assets/img/logo-black.png" width="100">
			</div>
			<div class="col-md-8 text-center">
				<b class="tnr" style="font-size: 18px;">KEMENTRIAN AGAMA REPUBLIK INDONESIA</b><br/>
				<b class="tnr" style="font-size: 18px;">UNIVERSITAS ISLAM NEGERI SUNAN GUNUNG DJATI BANDUNG</b><br/>
				<b class="tnr" style="font-size: 18px;">PUSAT TEKNOLOGI INFORMASI DAN PANGKALAN DATA</b>
				<p class="tnr">
					<small><?=getProfilCBT('alamat_profil');?></small><br/>
					<small>Email : <?=getProfilCBT('email_profil');?> No Telepon : <?=getProfilCBT('no_hp_profil');?></small>
				</p>
			</div>
			<div class="col-md-2 text-center">
				<br/>
				<img src="<?=base_url();?>assets/img/alur_daftar/<?=filterReport('alur_daftar', 'id', $alur_daftar, 'logo');?>" width="120">
			</div>
			<div class="col-md-12 text-center">
				<hr/>
			</div>
			
			<!-- Title -->
			<div class="col-md-2 text-left">
				<b>Lampiran</b>
			</div>
			<div class="col-md-4 text-left">
				<span>Data Peserta</span>
			</div>
			<div class="col-md-2 text-left">
				<b>Tanggal Dibuat</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=date('d F Y');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Fakultas</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('fakultas', 'id_fakultas', $fakultas, 'nama_fakultas');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Provinsi</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('provinsi', 'id', $provinsi, 'nama');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Jurusan</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('jurusan', 'id_jurusan', $jurusan, 'nama_jurusan');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Kota</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('kota', 'id_kota', $kota, 'nama_kota');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Jenis Kelamin</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=$jk;?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Alur Daftar</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('alur_daftar', 'id', $alur_daftar, 'nama_alur_daftar');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Status</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=$stat;?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Total</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=number_format($row);?></span>
			</div>
			
			<!-- Table -->
			<div class="col-md-12">
				<hr/>
				<table width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>NIM</th>
							<th>Nama</th>
							<th>Prodi</th>
							<th>Status Ayah</th>
							<th>Status Ibu</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($result as $R) { ?>
						<tr>
							<td><?=$no++;?></td>
							<td><?=$R->NIM;?></td>
							<td><?=strtoupper($R->nama_mhs);?></td>
							<td>(<?=$R->jurusan;?>) <?=filterReport('jurusan', 'id_jurusan', $R->jurusan, 'nama_jurusan');?></td>
							<td><?=$R->status_ayah;?></td>
							<td><?=$R->status_ibu;?></td>
							<td><?=$R->status_lulus;?></td>
						</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<div class="col-md-12"><hr/></div>
			<div class="col-md-6 text-left"></div>
			<div class="col-md-6 text-left">
				<p class="tnr"><?=getProfilCBT('kabupaten_profil').', '.date('d F Y');?></p>
				<p class="tnr" style="padding-bottom: 100px;"><b>Kepala Pusat Teknologi Informasi dan Pangkalan Data</b></p>
				<p class="tnr"><b><u>Undang Syaripudin, M.Kom.</u></b></p>
				<p class="tnr">197909302009121002</p>
			</div>
		</div>
	</div>
</body>
</html>
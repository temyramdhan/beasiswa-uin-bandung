<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Laporan Data Peserta <?=getProfilCBT('title_profil');?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?=base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?=base_url();?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">

	
	<!--   Core JS Files   -->
	<script src="<?=base_url();?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/bootstrap.min.js"></script>
	
	<style>
	table {
		border-collapse: collapse;
	}
	table, th, td {
		padding: 2px;
		border: 1px solid black;
		font-size: 12px;
	}	
	.A4 {
		width: 210mm;
		height: 297mm;
	}
	.tnr {
		font-family : 'Times New Roman';
	}
	h1 {
		padding:0; margin:0;
	}
	</style>

</head>
<body>
	<div class="container">
		<div class="row">
			<!-- Header -->
			<div class="col-md-2 text-center">
				<img src="<?=base_url();?>assets/img/logo-black.png" width="150">
			</div>
			<div class="col-md-8 text-center">
				<b class="tnr" style="font-size: 20px;">KEMENTRIAN AGAMA REPUBLIK INDONESIA</b><br/>
				<b class="tnr" style="font-size: 20px;">UNIVERSITAS ISLAM NEGERI SUNAN GUNUNG DJATI BANDUNG</b><br/>
				<b class="tnr" style="font-size: 20px;">PUSAT TEKNOLOGI INFORMASI DAN PANGKALAN DATA</b>
				<p class="tnr">
					<small><?=getProfilCBT('alamat_profil');?></small><br/>
					<small>Email : <?=getProfilCBT('email_profil');?> No Telepon : <?=getProfilCBT('no_hp_profil');?></small>
				</p>
			</div>
			<div class="col-md-2 text-center">
				<img src="<?=base_url();?>assets/img/alur_daftar/<?=filterReport('alur_daftar', 'id', $alur_daftar, 'logo');?>" width="120">
			</div>
			<div class="col-md-12 text-center">
				<hr/>
			</div>
			
			<!-- Title -->
			<div class="col-md-2 text-left">
				<b>Lampiran</b>
			</div>
			<div class="col-md-4 text-left">
				<span>Data Peserta</span>
			</div>
			<div class="col-md-2 text-left">
				<b>Tanggal Dibuat</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=date('d F Y');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Fakultas</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('fakultas', 'id_fakultas', $fakultas, 'nama_fakultas');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Provinsi</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('provinsi', 'id', $provinsi, 'nama');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Jurusan</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('jurusan', 'id_jurusan', $jurusan, 'nama_jurusan');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Kota</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('kota', 'id_kota', $kota, 'nama_kota');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Jenis Kelamin</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=$jk;?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Alur Daftar</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=filterReport('alur_daftar', 'id', $alur_daftar, 'nama_alur_daftar');?></span>
			</div>
			<div class="col-md-2 text-left">
				<b>Status</b>
			</div>
			<div class="col-md-4 text-left">
				<span><?=$stat;?></span>
			</div>
			
			<!-- Total -->
			<div class="col-md-12">
				<hr/>
			</div>
			<?php foreach($this->db->query('select * from fakultas')->result() as $f) { ?>
			<div class="col-md-6 text-left" style="padding-top: 10px;">
				<p style="margin:0; font-weight:bold;"><?=$f->nama_fakultas;?></p>
				<?php foreach($this->db->query('select * from jurusan where id_fakultas="'.$f->id_fakultas.'"')->result() as $k) { ?>
				<p style="margin:0; font-size:12px;"><?=$k->nama_jurusan;?></p>
				<?php } ?>
			</div>
			<?php } ?>
			</div>
		</div>
	</div>
</body>
</html>
		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold"><?=$title;?></h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-12">
							<div class="card card-annoucement card-round">
								<div class="card-body">
									<div class="table-responsive">
										<table id="table" class="display table table-striped table-hover">
											<thead>
												<tr>
													<th>No</th>
													<th>NIM</th>
													<th>Nama</th>
													<th>Tgl Daftar</th>
													<th>Kode Prodi</th>
													<th>Alur</th>
													<th class="text-center">Aksi</th>
												</tr>
											</thead>
											<tfoot></tfoot>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="card full-height">
								<div class="card-body">
									<div class="card-title"><i class="fas fa-filter"></i> Filter Laporan</div>
									<div class="card-category">Untuk mempersiapkan laporan diharuskan untuk memfilternya karena data lebih banyak.</div>
									<?=form_open('datapeserta/filter_report', $att);?>
									<div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="col-md-3">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Status</label>
													<select name="status" class="form-control form-control-sm">
														<option value="">Semua</option>
														<?php for($i = 0; $i < count($filter_status); $i++) { ?>
														<option value="<?=$filter_status[$i];?>"><?=$filter_status[$i];?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Jenis Kelamin</label>
													<select name="jk" class="form-control form-control-sm">
														<option value="">Semua</option>
														<?php for($i = 0; $i < count($filter_jk); $i++) { ?>
														<option value="<?=$filter_jk[$i];?>"><?=$filter_jk[$i];?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Fakultas</label>
													<select name="fakultas" id="fakultas" class="form-control form-control-sm" required>
														<option value="">Fakultas</option>
														<?php foreach($fakultas->result() as $F) { ?>
														<option value="<?=$F->id_fakultas;?>"><?=$F->nama_fakultas;?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Jurusan</label>
													<select name="jurusan" id="jurusan" class="form-control form-control-sm">
														<option value="">Semua Jurusan</option>
														<?php foreach($jurusan->result() as $F) { ?>
														<option value="<?=$F->id_jurusan;?>" data-chained="<?=$F->id_fakultas;?>"><?=$F->nama_jurusan;?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Provinsi</label>
													<select name="provinsi" id="provinsi" class="form-control form-control-sm">
														<option value="">Semua Provinsi</option>
														<?php foreach($provinsi->result() as $F) { ?>
														<option value="<?=$F->id;?>"><?=$F->nama;?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Kota</label>
													<select name="kota" id="kota" class="form-control form-control-sm">
														<option value="" data-chained="">Semua Kota</option>
														<?php foreach($kota->result() as $F) { ?>
														<option value="<?=$F->id_kota;?>" data-chained="<?=$F->id_provinsi_fk;?>"><?=$F->nama_kota;?></option>
														<?php } ?>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group" style="padding-left:0;">
													<label for="smallSelect">Alur Daftar</label>
													<select name="alur_daftar" class="form-control form-control-sm" required>
														<option value="">Alur Daftar</option>
														<?php foreach($alur_daftar->result() as $F) { ?>
														<option value="<?=$F->id;?>"><?=$F->nama_alur_daftar;?></option>
														<?php } ?>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-12">
										<div class="selectgroup w-100">
											<label class="selectgroup-item">
												<input type="radio" name="filetype" value="pdf" class="selectgroup-input" checked="">
												<span class="selectgroup-button">PDF</span>
											</label>
											<label class="selectgroup-item">
												<input type="radio" name="filetype" value="xls" class="selectgroup-input">
												<span class="selectgroup-button">XLS</span>
											</label>
											<!--
											<label class="selectgroup-item">
												<input type="radio" name="filetype" value="cover" class="selectgroup-input">
												<span class="selectgroup-button">Buat Cover</span>
											</label>
											-->
										</div>
										<button type="submit" class="btn btn-secondary btn-block" style="margin-top: 12px">
											Filter
										</button>
									</div>
									</div>
									<?=form_close();?>
								</div>
							</div>
						</div>
						<script>
					$(document).ready(function() {
						<?=$this->session->flashdata('notif');?>
						$('#kota').chained('#provinsi');
						$('#jurusan').chained('#fakultas');
						$.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
						{
							return {
								"iStart": oSettings._iDisplayStart,
								"iEnd": oSettings.fnDisplayEnd(),
								"iLength": oSettings._iDisplayLength,
								"iTotal": oSettings.fnRecordsTotal(),
								"iFilteredTotal": oSettings.fnRecordsDisplay(),
								"iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
								"iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
							};
						};	
						$('#table').DataTable({
							"initComplete": function() {
								var api = this.api();
									$('#table_filter input')
										.off('.DT')
										.on('keyup.DT', function(e) {
											if (e.keyCode == 13) {
												api.search(this.value).draw();
											}
										});
							},
							"pageLength": 5,
							"lengthMenu": [5, 10, 20, 50, 100, 500, 1000],
							"processing": true,
							"serverSide": true,
							"order": [],
							"ajax": {
								"url": "<?=base_url();?>Datapeserta/jsonDatapeserta",
								"type": "POST"
							},
							"oLanguage": {
								sProcessing: "Tunggu Sebentar"
							},
							"columns": [
								{"data" : "NIM"},
								{"data" : "NIM"},
								{"data" : "nama"},
								{"data" : "tgl_daftar"},
								{"data" : "jurusan"},
								{"data" : "alur_daftar"},
								{"data" : "aksi", "orderable": false}
							],
							rowCallback: function(row, data, iDisplayIndex) {
								var info = this.fnPagingInfo();
								var page = info.iPage;
								var length = info.iLength;
								var index = page * length + (iDisplayIndex + 1);
								$('td:eq(0)', row).html(index);
							}
						});
					});
						</script>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
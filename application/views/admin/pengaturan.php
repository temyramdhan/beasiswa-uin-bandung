		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold"><?=$title;?></h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-12">
							<div class="card">
								<div class="card-body">
									<div class="row py-4">
										<div class="col-md-3">
											<div class="nav flex-column nav-pills nav-default nav-pills-no-bd" role="tablist" aria-orientation="vertical">
												<a class="nav-link active" data-toggle="tab" href="#alur_daftar" role="tab" aria-controls="alur_daftar" aria-selected="false">Alur Daftar</a>
												<a class="nav-link" data-toggle="tab" href="#maintenance" role="tab" aria-controls="maintenance" aria-selected="false">Maintenance</a>
												<a class="nav-link" data-toggle="tab" href="#backup_db" role="tab" aria-controls="backup_db" aria-selected="false">Back Up Database</a>
												<a class="nav-link" data-toggle="tab" href="#tampilan" role="tab" aria-controls="tampilan" aria-selected="false">Tampilan</a>
											</div>
										</div>
										<div class="col-md-9">
											<div class="tab-content" id="">
												<div class="tab-pane fade show active" id="alur_daftar" role="tabpanel" aria-labelledby="pills-home-tab">
													<div class="row">
														<div class="col-md-12 text-left" style="padding-bottom: 15px;">
															<div class="form-group">
																<label for="">Alur Daftar</label>
																<form method="post" action="<?=base_url();?>Pengaturan">
																<div class="input-group">
																	<select class="form-control form-control-sm" name="alur_daftar" required>
																		<option value="">Alur Daftar</option>
																		<?php 
																		foreach($alur_daftar->result() as $AlurDaftar) {
																			if($this->input->post('alur_daftar') == $AlurDaftar->id) {
																				$selected = "selected";
																			}else{
																				$selected = "";
																			}
																		?>
																		<option value="<?=$AlurDaftar->id;?>" <?=$selected;?>><?=$AlurDaftar->nama_alur_daftar;?></option>
																		<?php } ?>
																		<option value="add_alur_daftar">Tambah Alur Daftar</option>
																	</select>
																	<button type="submit" class="btn btn-success btn-sm" style="border-radius:0;">
																		<i style="font-size: 15px;" class="fas fa-check"></i>
																	</button>
																</div>
																</form>
															</div>
														</div>
														<div class="col-md-12 text-left" style="padding-bottom: 15px;">
															<?php if(!$this->input->post('alur_daftar')) { ?>
															<div class="alert alert-danger">
																Anda belum menentukan alur daftar.
															</div>
															<?php } else {
																	if($this->input->post('alur_daftar') == "add_alur_daftar") {
																		$this->load->view('admin/pengaturan/alur_daftar_baru');
																	}else{
																		$this->load->view('admin/pengaturan/alur_daftar');
																	}
															} ?>
														</div>
													</div>
												</div>
												<div class="tab-pane fade" id="maintenance" role="tabpanel" aria-labelledby="pills-home-tab">
													<div class="row">
														<div class="col-md-12 text-center" style="padding-bottom: 15px;">
															<h3 class="" style="font-weight:bold;">Coming Soon</h3>
														</div>
													</div>
												</div>
												<div class="tab-pane fade" id="tampilan" role="tabpanel" aria-labelledby="pills-home-tab">
													<div class="row">
														<div class="col-md-12 text-left" style="padding-bottom: 5px;">
															<label style="font-weight:bold;">Tampilan</label>
														</div>
														<div class="col-md-6 text-left" style="padding-bottom: 15px;">
															<div class="form-group">
																<label class="form-label">Admin</label>
																<div class="row gutters-xs">
																	<div class="col-auto">
																		<label class="colorinput">
																			<input name="color" type="radio" value="warning" class="colorinput-input">
																			<span class="colorinput-color bg-warning-gradient"></span>
																		</label>
																	</div>
																	<div class="col-auto">
																		<label class="colorinput">
																			<input name="color" type="radio" value="warning" class="colorinput-input">
																			<span class="colorinput-color bg-warning"></span>
																		</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
		
		<script type="text/javascript">
		<?=$this->session->flashdata('notif');?>
		</script>
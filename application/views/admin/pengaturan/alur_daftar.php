													<?php foreach(getAlurDaftar($this->input->post('alur_daftar'))->result() as $AD) { ?>
													<div class="row">
														<div class="col-md-12 text-center" style="padding-bottom: 15px;">
															<p><img src="<?=base_url();?>assets/img/alur_daftar/<?=filterReport('alur_daftar', 'id', $AD->id, 'logo');?>" width="80"></p>
															<h3 class="" style="font-weight:bold;">Beasiswa <?=$AD->nama_alur_daftar;?></h3>
															<small style="margin-top: 12px;">Dibuat & Diperbaharui pada tanggal : <b><?=date('d F Y H:i', strtotime($AD->tgl));?></b></small>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Program Studi (Prodi)</label>
																<?php foreach(ProdiAlurDaftar($AD->id)->result() as $PAD) { ?>
																<form method="post" action="<?=base_url();?>Pengaturan/update_prodi_alur_daftar/<?=$PAD->id;?>">
																<div class="input-group">
																	<input type="hidden" name="alur_daftar" value="<?=$AD->id;?>">
																	<button type="submit" class="btn btn-secondary btn-sm" style="border-radius:0;">
																		<i style="font-size: 15px;" class="fas fa-save"></i>
																	</button>
																	<a href="<?=base_url();?>Pengaturan/hapus_prodi_alur_daftar/<?=$PAD->id;?>" class="btn btn-danger btn-sm" style="border-radius:0;">
																		<i style="font-size: 15px;" class="fas fa-trash-alt"></i>
																	</a>
																	<input type="hidden" name="prodi_alur_daftar" value="<?=$PAD->alur_daftar;?>">
																	<select class="form-control form-control-sm" name="prodi" required>
																		<?php 
																		foreach($jur->result() as $Prodi) { 
																		if($Prodi->id_jurusan == $PAD->prodi) { $select = "selected"; } else { $select = ""; }
																		?>
																		<option value="<?=$Prodi->id_jurusan;?>" <?=$select;?>><?=$Prodi->id_jurusan." - ".$Prodi->nama_jurusan;?></option>
																		<?php } ?>
																	</select>
																</div>
																</form>
																<?php } ?>
																<form method="post" action="<?=base_url();?>Pengaturan/tambah_prodi_alur_daftar">
																<input type="hidden" name="alur_daftar" value="<?=$AD->id;?>">
																<div class="input-group">
																	<button type="submit" class="btn btn-default btn-sm" style="border-radius:0;">
																		<i style="font-size: 15px;" class="fas fa-plus"></i>
																	</button>
																	<input type="hidden" name="prodi_alur_daftar" value="<?=$AD->id;?>">
																	<select class="form-control form-control-sm" name="prodi" required>
																		<option value="">Tambah Prodi</option>
																		<?php 
																		foreach($jur->result() as $Prodi) {
																		?>
																		<option value="<?=$Prodi->id_jurusan;?>"><?=$Prodi->id_jurusan." - ".$Prodi->nama_jurusan;?></option>
																		<?php } ?>
																	</select>
																</div>
																</form>
															</div>
														</div>
														<div class="col-md-12">
															<form method="post" enctype="multipart/form-data" action="<?=base_url();?>Pengaturan/update_alur_daftar">
															<input type="hidden" name="id" value="<?=$AD->id;?>">
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Nama Alur Daftar</label>
																<input type="text" class="form-control" name="nama_alur_daftar" placeholder="Nama Alur Daftar" value="<?=filterReport('alur_daftar', 'id', $AD->id, 'nama_alur_daftar');?>">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-group form-group-default">
																<label for="">Minimal IPK</label>
																<input type="text" class="form-control" name="ipk" placeholder="Minimal IPK" value="<?=filterReport('alur_daftar', 'id', $AD->id, 'min_ipk');?>">
															</div>
														</div>
														<div class="col-md-5">
															<div class="form-group form-group-default">
																<label for="">Upload Logo</label>
																<input type="file" class="form-control" name="logo" placeholder="Logo" value="">
															</div>
														</div>
														<div class="col-md-5">
															<div class="form-group form-group-default">
																<label for="">Link</label>
																<input type="text" class="form-control" name="link" placeholder="Link" value="<?=filterReport('alur_daftar', 'id', $AD->id, 'link');?>">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pendaftaran Mulai</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pendaftaran_mulai_<?=$AD->id;?>" class="form-control" name="tgl_pendaftaran_mulai" placeholder="Tanggal Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pendaftaran_mulai'), 0, 10);?>">
																	<input type="time" class="form-control" name="wkt_pendaftaran_mulai" placeholder="Waktu Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pendaftaran_mulai'), 11, 5);?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pendaftaran Berakhir</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pendaftaran_berakhir_<?=$AD->id;?>" class="form-control" name="tgl_pendaftaran_berakhir" placeholder="Tanggal Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pendaftaran_berakhir'), 0, 10);?>">
																	<input type="time" class="form-control" name="wkt_pendaftaran_berakhir" placeholder="Waktu Pendaftaran Berakhir" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pendaftaran_berakhir'), 11, 5);?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumpulan Mulai</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumpulan_mulai_<?=$AD->id;?>" class="form-control" name="tgl_pengumpulan_mulai" placeholder="Tanggal Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumpulan_mulai'), 0, 10);?>">
																	<input type="time" class="form-control" name="wkt_pengumpulan_mulai" placeholder="Waktu Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumpulan_mulai'), 11, 5);?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumpulan Berakhir</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumpulan_berakhir_<?=$AD->id;?>" class="form-control" name="tgl_pengumpulan_berakhir" placeholder="Tanggal Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl'), 0, 10);?>">
																	<input type="time" class="form-control" name="wkt_pengumpulan_berakhir" placeholder="Waktu Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumpulan_berakhir'), 11, 5);?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumuman Kelulusan Mulai</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumuman_kelulusan_mulai_<?=$AD->id;?>" class="form-control" name="tgl_pengumuman_kelulusan_mulai" placeholder="Tanggal Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumuman_kelulusan_mulai'), 0, 10);?>">
																	<input type="time" class="form-control" name="wkt_pengumuman_kelulusan_mulai" placeholder="Waktu Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumuman_kelulusan_mulai'), 11, 5);?>">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumuman Kelulusan Berakhir</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumuman_kelulusan_berakhir_<?=$AD->id;?>" class="form-control" name="tgl_pengumuman_kelulusan_berakhir" placeholder="Tanggal Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumuman_kelulusan_berakhir'), 0, 10);?>">
																	<input type="time" class="form-control" name="wkt_pengumuman_kelulusan_berakhir" placeholder="Waktu Pendaftaran Mulai" value="<?=substr(filterReport('alur_daftar', 'id', $AD->id, 'tgl_pengumuman_kelulusan_berakhir'), 11, 5);?>">
																</div>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Kriteria</label>
																<textarea id="kriteria_<?=$AD->id;?>" placeholder="Kriteria" name="kriteria" rows="5" required>
																<?=filterReport('alur_daftar', 'id', $AD->id, 'kriteria');?>
																</textarea>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Persyaratan</label>
																<textarea id="persyaratan_<?=$AD->id;?>" placeholder="Persyaratan" name="persyaratan" rows="5" required>
																<?=filterReport('alur_daftar', 'id', $AD->id, 'persyaratan');?>
																</textarea>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Seleksi</label>
																<textarea id="seleksi_<?=$AD->id;?>" placeholder="Persyaratan" name="seleksi" rows="5" required>
																<?=filterReport('alur_daftar', 'id', $AD->id, 'seleksi');?>
																</textarea>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Deskripsi</label>
																<textarea id="deskripsi_<?=$AD->id;?>" placeholder="Deskripsi" name="deskripsi" rows="5" required>
																<?=filterReport('alur_daftar', 'id', $AD->id, 'deskripsi');?>
																</textarea>
															</div>
														</div>
														<div class="col-md-6">
															<button class="btn btn-success btn-border btn-block" type="submit">
																<span class="btn-label">
																	<i class="fas fa-save"></i>
																</span>
																Simpan
															</button>
														</div>
														<div class="col-md-6">
															<button class="btn btn-secondary btn-border btn-block" type="reset">
																<span class="btn-label">
																	<i class="fas fa-redo"></i>
																</span>
																Reset
															</button>
														</div>
														<div class="col-md-12">
															</form>
														</div>
													</div>
													<script type="text/javascript">
												ClassicEditor
													.create( document.querySelector( '#kriteria_<?=$AD->id;?>' ) )
													.catch( error => {
														console.error( error );
													} );
												ClassicEditor
													.create( document.querySelector( '#persyaratan_<?=$AD->id;?>' ) )
													.catch( error => {
														console.error( error );
													} );
												ClassicEditor
													.create( document.querySelector( '#seleksi_<?=$AD->id;?>' ) )
													.catch( error => {
														console.error( error );
													} );
												ClassicEditor
													.create( document.querySelector( '#deskripsi_<?=$AD->id;?>' ) )
													.catch( error => {
														console.error( error );
													} );
												</script>
												<?php } ?>
<div class="row">
														<div class="col-md-12 text-center" style="padding-bottom: 15px;">
															<h3 class="" style="font-weight:bold;">Tambah Alur Daftar Beasiswa</h3>
															<small style="margin-top: 12px;">Silahkan untuk menabah alur daftar beasiswa</b></small>
														</div>
														<div class="col-md-12">
															<form method="post" enctype="multipart/form-data" action="<?=base_url();?>Pengaturan/tambah_alur_daftar">
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Kode Alur Daftar</label>
																<input type="text" class="form-control" name="id" placeholder="Kode Alur Daftar">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Nama Alur Daftar</label>
																<input type="text" class="form-control" name="nama_alur_daftar" placeholder="Nama Alur Daftar">
															</div>
														</div>
														<div class="col-md-2">
															<div class="form-group form-group-default">
																<label for="">Minimal IPK</label>
																<input type="text" class="form-control" name="ipk" placeholder="Minimal IPK">
															</div>
														</div>
														<div class="col-md-5">
															<div class="form-group form-group-default">
																<label for="">Upload Logo</label>
																<input type="file" class="form-control" name="logo" placeholder="Logo">
															</div>
														</div>
														<div class="col-md-5">
															<div class="form-group form-group-default">
																<label for="">Link</label>
																<input type="text" class="form-control" name="link" placeholder="Link">
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pendaftaran Mulai</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pendaftaran_mulai" class="form-control" name="tgl_pendaftaran_mulai">
																	<input type="time" class="form-control" name="wkt_pendaftaran_mulai">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pendaftaran Berakhir</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pendaftaran_berakhir" class="form-control" name="tgl_pendaftaran_berakhir">
																	<input type="time" class="form-control" name="wkt_pendaftaran_berakhir">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumpulan Mulai</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumpulan_mulai" class="form-control" name="tgl_pengumpulan_mulai">
																	<input type="time" class="form-control" name="wkt_pengumpulan_mulai">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumpulan Berakhir</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumpulan_berakhir" class="form-control" name="tgl_pengumpulan_berakhir">
																	<input type="time" class="form-control" name="wkt_pengumpulan_berakhir">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumuman Kelulusan Mulai</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumuman_kelulusan_mulai" class="form-control" name="tgl_pengumuman_kelulusan_mulai">
																	<input type="time" class="form-control" name="wkt_pengumuman_kelulusan_mulai">
																</div>
															</div>
														</div>
														<div class="col-md-6">
															<div class="form-group form-group-default">
																<label for="">Tanggal Pengumuman Kelulusan Berakhir</label>
																<div class="input-group">
																	<input type="date" data-date-format="YYYY-MM-DD" id="tgl_pengumuman_kelulusan_berakhir" class="form-control" name="tgl_pengumuman_kelulusan_berakhir">
																	<input type="time" class="form-control" name="wkt_pengumuman_kelulusan_berakhir">
																</div>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Kriteria</label>
																<textarea id="kriteria" placeholder="Kriteria" name="kriteria" rows="5" required>
																</textarea>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Persyaratan</label>
																<textarea id="persyaratan" placeholder="Persyaratan" name="persyaratan" rows="5" required>
																</textarea>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Seleksi</label>
																<textarea id="seleksi" placeholder="Seleksi" name="seleksi" rows="5" required>
																</textarea>
															</div>
														</div>
														<div class="col-md-12">
															<div class="form-group form-group-default">
																<label for="">Deskripsi</label>
																<textarea id="deskripsi" placeholder="Deskripsi" name="deskripsi" rows="5" required>
																</textarea>
															</div>
														</div>
														<div class="col-md-6">
															<button class="btn btn-success btn-border btn-block" type="submit">
																<span class="btn-label">
																	<i class="fas fa-save"></i>
																</span>
																Simpan
															</button>
														</div>
														<div class="col-md-6">
															<button class="btn btn-secondary btn-border btn-block" type="reset">
																<span class="btn-label">
																	<i class="fas fa-redo"></i>
																</span>
																Reset
															</button>
														</div>
														<div class="col-md-12">
															</form>
														</div>
													</div>
													<script>
												ClassicEditor
													.create( document.querySelector( '#kriteria' ) )
													.catch( error => {
														console.error( error );
													} );
												ClassicEditor
													.create( document.querySelector( '#persyaratan' ) )
													.catch( error => {
														console.error( error );
													} );
												ClassicEditor
													.create( document.querySelector( '#seleksi' ) )
													.catch( error => {
														console.error( error );
													} );
												ClassicEditor
													.create( document.querySelector( '#deskripsi' ) )
													.catch( error => {
														console.error( error );
													} );
												<?=$this->session->flashdata('notif');?>
												</script>
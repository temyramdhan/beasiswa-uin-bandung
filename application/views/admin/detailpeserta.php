		<div class="main-panel">
			<div class="content">
				<div class="panel-header" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center; background-size: cover;">
					<div class="page-inner py-4">
						<div class="">
							<div>
								<h2 class="text-white text-center fw-bold"><?=$title;?></h2>
								<h5 class="text-white text-center op-7 mb-4"><?=getProfilCBT('title_profil');?></h5>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<?php foreach($detail as $D) { ?>
						<div class="col-md-8">
							<div class="card card-with-nav">
								<div class="card-header">
									<div class="row row-nav-line">
										<ul class="nav nav-tabs nav-line nav-color-secondary w-100 pl-3" role="tablist">
											<li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#step1" role="tab" aria-selected="true">Biodata</a> </li>
											<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#step2" role="tab" aria-selected="false">Orang Tua</a> </li>
											<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#step3" role="tab" aria-selected="false">Seputar Beasiswa</a> </li>
											<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#step4" role="tab" aria-selected="false">Lampiran</a> </li>
										</ul>
									</div>
								</div>
								<div class="card-body">
									<div class="tab-content mt-2 mb-3" id="">
										<div class="tab-pane fade show active" id="step1" role="tabpanel" aria-labelledby="pills-home-tab">
											<div class="row">
												<?php $this->load->view('admin/step/1'); ?>
											</div>
										</div>
										<div class="tab-pane fade" id="step2" role="tabpanel" aria-labelledby="pills-home-tab">
											<div class="row">
												<?php $this->load->view('admin/step/2'); ?>
											</div>	
										</div>
										<div class="tab-pane fade" id="step3" role="tabpanel" aria-labelledby="pills-home-tab">
											<?php if($D->periode < 2020) { ?>
											<i class="text-center">Belum ada seputar beasiswa untuk periode ini.</i>
											<?php } else { ?>
											<div class="row">
												<?php $this->load->view('admin/step/3'); ?>
											</div>
											<?php } ?>
										</div>
										<div class="tab-pane fade" id="step4" role="tabpanel" aria-labelledby="pills-home-tab">
											D
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="card card-profile">
								<div class="card-header bg-secondary-gradient">
									<div class="profile-picture">
										<div class="avatar avatar-xl">
											<img src="<?=base_url();?>assets/img/lampiran/mahasiswa/<?=$D->foto_mhs;?>" style="padding:-20px;" class="avatar-img rounded-circle">
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="user-profile text-center">
										<div class="name"><?=$D->NIM;?></div>
										<div class="job"><?=$D->nama_mhs;?></div>
										<div class="desc"><?=StatusVerifikasi($D->NIM, $D->alur_daftar, $D->periode);?></div>
									</div>
								</div>
								<div class="card-footer">
									<div class="row user-stats text-center">
										<div class="col">
											<div class="title">Periode</div>
											<div class="number"><small><?=$D->periode;?></small></div>
										</div>
										<div class="col">
											<div class="title">Alur Daftar</div>
											<div class="number"><small><?=$D->alur_daftar;?></small></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<script>
						$('#kota').chained('#provinsi');
						$('#kota_ortu').chained('#provinsi_ortu');
						</script>
					</div>
				</div>
			</div>
			<?php $this->load->view('./template/copyright'); ?>
		</div>
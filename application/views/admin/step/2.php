										<div class="col-md-12">
											<form method="post" action="<?=base_url()."/".$url;?>/step2/<?=$id;?>">
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Nama Lengkap Ayah</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'nama_ayah', $periode);?>" type="text" class="form-control" id="" placeholder="Nama Lengkap Ayah" name="nama_ayah" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Nama Lengkap Ibu</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'nama_ibu', $periode);?>" type="text" class="form-control" id="" placeholder="Nama Lengkap Ibu" name="nama_ibu" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Provinsi Orangtua</label>
												<select class="form-control" id="provinsi_ortu" placeholder="Provinsi" name="provinsi_ortu" required>
													<option value="">Pilih Provinsi</option>
													<?php foreach($provinsi->result() as $Prov) { ?>
													<option value="<?=$Prov->id;?>" <?=ProvinsiOrtu($nim, $kode_alur_daftar, $Prov->id, $periode);?>><?=$Prov->nama;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Kota Orangtua</label>
												<select class="form-control" id="kota_ortu" placeholder="Kota" name="kota_ortu" required>
													<option value="" data-chained="">Pilih Kota</option>
													<?php foreach($kota->result() as $Kota) { ?>
													<option value="<?=$Kota->id_kota;?>" data-chained="<?=$Kota->id_provinsi_fk;?>" <?=KotaOrtu($nim, $kode_alur_daftar, $Kota->id_kota, $periode);?>><?=$Kota->nama_kota;?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">No Telepon Orangtua</label>
												<input value="<?=txtBeasiswa($nim, $kode_alur_daftar, 'no_telepon_ortu', $periode);?>" type="text" class="form-control" id="" placeholder="No Telepon Orangtua" name="no_telp_ortu" required>
												<small class="form-text text-muted">Pastikan nomor telepon bisa dihubungi.</small>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Status Ayah</label>
												<div class="selectgroup w-100">
													<?php for($i = 0; $i < count($status); $i++) { ?>
													<label class="selectgroup-item">
														<input type="radio" name="status_ayah" value="<?=$status[$i];?>" class="selectgroup-input" <?=StatusHM($nim, $kode_alur_daftar, 'status_ayah', $status[$i], $periode);?> required>
														<span class="selectgroup-button"><?=$status[$i];?></span>
													</label>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Status Ibu</label>
												<div class="selectgroup w-100">
													<?php for($i = 0; $i < count($status); $i++) { ?>
													<label class="selectgroup-item">
														<input type="radio" name="status_ibu" value="<?=$status[$i];?>" class="selectgroup-input" <?=StatusHM($nim, $kode_alur_daftar, 'status_ibu', $status[$i], $periode);?> required>
														<span class="selectgroup-button"><?=$status[$i];?></span>
													</label>
													<?php } ?>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group form-group-default">
												<label for="">Pekerjaan Orangtua</label>
												<select class="form-control" id="pekerjaan" placeholder="Pekerjaan" name="pekerjaan" required>
													<option value="">Pilih Pekerjaan</option>
													<?php for($i = 0; $i < count($pekerjaan); $i++) { ?>
													<option value="<?=$pekerjaan[$i];?>" <?=cmbBeasiswa($nim, $kode_alur_daftar, 'pekerjaan_orangtua', $pekerjaan[$i], $periode);?>><?=$pekerjaan[$i];?></option>
													<?php } ?>
												</select>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group form-group-default">
												<label for="">Alamat Orangtua Sekarang</label>
												<textarea class="form-control" id="" placeholder="Alamat Orangtua Sekarang" name="alamat" rows="5" required><?=txtBeasiswa($nim, $kode_alur_daftar, 'alamat_ortu', $periode);?></textarea>
											</div>
										</div>
										<div class="col-md-12">
											<button class="btn btn-success btn-border btn-block" type="submit">
												<span class="btn-label">
													<i class="fas fa-save"></i>
												</span>
												Simpan
											</button>
										</div>
										<div class="col-md-12">
											</form>
										</div>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?=getProfilCBT('title_profil');?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?=base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?=base_url();?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<style>
	.logo { filter: grayscale(100%); }
	.logo:hover { filter: grayscale(0%); cursor: pointer; }
	.close { cursor: pointer; }
	.close:hover { background-color: green; }
	.close:active { background-color: green; }
	</style>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/atlantis.min2.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/demo.css">
	
	<!--   Core JS Files   -->
	<script src="<?=base_url();?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/popper.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?=base_url();?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


	<!-- Chart JS -->
	<script src="<?=base_url();?>assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="<?=base_url();?>assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="<?=base_url();?>assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="<?=base_url();?>assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="<?=base_url();?>assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="<?=base_url();?>assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?=base_url();?>assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Sweet Alert 2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	
	<!-- Pace JS -->
	<script src="<?=base_url();?>assets/plugin/pace/pace.js"></script>
	<link rel="stylesheet" href="<?=base_url();?>assets/plugin/pace/themes/green/pace-theme-corner-indicator.css">
	
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/owl.theme.default.min.css">
	<script src="<?=base_url();?>assets/js/owl.carousel.min.js"></script>

	<!-- Typewritter JS -->
	<script src="<?=base_url();?>assets/plugin/typewriterjs/dist/core.js"></script>
	
	<!-- Atlantis JS -->
	<script src="<?=base_url();?>assets/js/atlantis.min.js"></script>
	
	<script>
	$(document).ready(function() {
		$('#opening').owlCarousel({
			items:4,
			loop:true,
			margin:30,
			autoplay:true,
			autoplayTimeout:2000,
			autoplayHoverPause:true
		});
		const instance = new Typewriter('#sub-judul', {
			autoStart: true,
			loop: true,
			cursor: ""
		});
		instance
			.typeString("Selamat Datang di Portal <?=getProfilCBT('title_profil');?>")
			.pauseFor(1000)
			.deleteAll(15)
			.typeString("Email : <?=getProfilCBT('email_profil');?>")
			.pauseFor(1000)
			.deleteAll(15)
			.typeString("No Telepon : <?=getProfilCBT('no_hp_profil');?>")
			.pauseFor(1000)
			.deleteAll(15)
			.typeString("<?=getProfilCBT('alamat_profil');?>")
			.pauseFor(1000)
			.deleteAll(15)
			.start();
	});
	</script>

</head>
<body class="login">
	<div class="wrapper wrapper-login wrapper-login-full p-0">
		<div class="login-aside w-50 align-items-center d-flex flex-column justify-content-center text-center animated fadeIn" style="background: url('<?=base_url();?>assets/img/bg.jpg') no-repeat fixed center bottom; background-size: cover;">
			<img class="py-3" src="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" width="130" height="auto" />
			<h2 class="fw-bold text-white mb-3">Portal <?=getProfilCBT('title_profil');?></h2>
			<span id="sub-judul" class="text-center text-white"></span>
		</div>
		<div class="login-aside w-50 d-flex align-items-center justify-content-center bg-white">
			<div class="container container-login container-transparent animated fadeIn">
				<div class="login-form" style="margin-top: -50px;">
					<center>
						<h3 style="margin:0;">Masuk menggunakan akun SIMAK/SALAM</h3>
					</center>
					<form method="post" action="<?=base_url();?>LoginUser/Auth">
					<div class="form-group">
						<label for="nim" class="placeholder"><b>NIM</b></label>
						<input id="nim" placeholder="NIM" name="nim" type="text" class="form-control" required>
					</div>
					<div class="form-group">
						<label for="password" class="placeholder"><b>Password</b></label>
						<div class="position-relative">
							<input id="password" placeholder="Password" name="password" type="password" class="form-control" required>
						</div>
					</div>
					<div class="form-group form-action-d-flex mb-3">
						<button type="submit" class="btn btn-success col-md-12 float-right mt-3 mt-sm-0 fw-bold">Masuk</button>
					</div>
					</form>
				</div>
				<div id="opening" class="owl-carousel" style="padding-top: 15px;">
					<?php foreach($alur_daftar->result() as $AD) { ?>
					<div class="item" data-toggle="tooltip" data-placement="bottom" title="Beasiswa <?=$AD->nama_alur_daftar;?>">
						<img data-toggle="modal" data-target="#logo_<?=$AD->id;?>" class="logo" src="<?=base_url();?>assets/img/alur_daftar/<?=$AD->logo;?>" width="64" height="auto" />
					</div>
					<?php } ?>
				</div>
				<center>
					<span style="font-size: 12px;">Copyright &copy; Pusat Teknologi Informasi dan Pangkalan Data UIN Sunan Gunung Djati Bandung.</span>
				</center>
			</div>
		</div>
	</div>
	
	<script>
	<?=$this->session->flashdata('notif');?>
	</script>
	
	<?php foreach($alur_daftar->result() as $AD) { ?>
	<div class="modal" id="logo_<?=$AD->id;?>" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-body bg-white text-black">
					<h3 class="text-center">Beasiswa <?=$AD->nama_alur_daftar;?></h3>
					<hr/>
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-3 py-3" align="center">
								<img src="<?=base_url();?>assets/img/alur_daftar/<?=$AD->logo;?>" width="128" height="auto" />
							</div>
							<div class="col-md-9 py-3"><?=$AD->deskripsi;?></div>
						</div>
						<div class="row">
							<div class="col-md-12 py-3">
								<p style="margin:0;">
									<span class="fw-bold">Mulai Pendaftaran</span>
									<span class="pull-right"><?=strftime('%d %B %Y', strtotime($AD->tgl_pendaftaran_mulai));?></span>
								</p>
								<p style="margin:0;">
									<span class="fw-bold">Akhir Pendaftaran</span>
									<span class="pull-right"><?=strftime('%d %B %Y', strtotime($AD->tgl_pendaftaran_berakhir));?></span>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-body bg-success-gradient text-white text-center close" data-dismiss="modal">
					<i class="far fa-times-circle" style="font-size: 30px;"></i>
				</div>
			</div>
		</div>
	</div>
	<?php } ?>
</body>
</html>

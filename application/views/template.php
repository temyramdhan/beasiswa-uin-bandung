<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?=$title;?> - <?=getProfilCBT('title_profil');?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?=base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?=base_url();?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/atlantis.min.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/demo.css">
	
	<!--   Core JS Files   -->
	<script src="<?=base_url();?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/popper.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?=base_url();?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>
	
	<!-- jQuery Chained -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-chained/jquery.chained.js"></script>

	<!-- Chart JS -->
	<script src="<?=base_url();?>assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="<?=base_url();?>assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="<?=base_url();?>assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="<?=base_url();?>assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="<?=base_url();?>assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="<?=base_url();?>assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?=base_url();?>assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Sweet Alert 2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	
	<!-- Pace -->
	<script src="<?=base_url();?>assets/plugin/pace/pace.js"></script>
	<link rel="stylesheet" href="<?=base_url();?>assets/plugin/pace/themes/green/pace-theme-corner-indicator.css">

	<!-- Atlantis JS -->
	<script src="<?=base_url();?>assets/js/atlantis.min.js"></script>
	
	<script>
		Pace.restart();
		paceOptions = {
			elements: true
		};
	</script>
</head>
<body data-background-color="white">
	<div class="wrapper">
		<div class="main-header">
			<!-- Logo Header -->
			<div class="logo-header bg-success-gradient" data-background-color="">
				
				<a href="<?=base_url();?>" class="logo">
					<img src="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" width="50" alt="navbar brand" class="navbar-brand">
				</a>
				<button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon">
						<i class="icon-menu"></i>
					</span>
				</button>
				<button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
				<div class="nav-toggle">
					<button class="btn btn-toggle toggle-sidebar">
						<i class="icon-menu"></i>
					</button>
				</div>
			</div>
			<!-- End Logo Header -->

			<!-- Navbar Header -->
			<nav class="navbar navbar-header navbar-expand-lg bg-success-gradient" data-background-color="">
				
				<div class="container-fluid">
					
					<ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
						<li class="nav-item dropdown hidden-caret">
							<a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
								<div class="avatar-sm">
									<img src="<?=base_url();?>assets/img/<?=getFotoProfil($nim);?>" class="avatar-img rounded-circle">
								</div>
							</a>
							<ul class="dropdown-menu dropdown-user animated fadeIn">
								<div class="dropdown-user-scroll scrollbar-outer">
									<li>
										<div class="user-box">
											<div class="avatar-lg"><img src="<?=base_url();?>assets/img/<?=getFotoProfil($nim);?>" alt="image profile" class="avatar-img rounded"></div>
											<div class="u-text">
												<h4><?=Data_SALAM($nim, 'mhsNama');?></h4>
												<p class="text-muted"><?=Data_SALAM($nim, 'mhsNiu');?></p>
											</div>
										</div>
									</li>
								</div>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
			<!-- End Navbar -->
		</div>

		<!-- Sidebar -->
		<?php $this->load->view("template/sidebar"); ?>
		<!-- End Sidebar -->

		<?=$contents;?>
		
	</div>
</body>
</html>

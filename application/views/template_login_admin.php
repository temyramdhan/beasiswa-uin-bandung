<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Admin Panel <?=getProfilCBT('title_profil');?></title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" type="image/x-icon"/>

	<!-- Fonts and icons -->
	<script src="<?=base_url();?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Lato:300,400,700,900"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands", "simple-line-icons"], urls: ['<?=base_url();?>assets/css/fonts.min.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
		$('#opening').carousel();
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/css/atlantis.min.css">

	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link rel="stylesheet" href="<?=base_url();?>assets/css/demo.css">
	
	<!--   Core JS Files   -->
	<script src="<?=base_url();?>assets/js/core/jquery.3.2.1.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/popper.min.js"></script>
	<script src="<?=base_url();?>assets/js/core/bootstrap.min.js"></script>

	<!-- jQuery UI -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?=base_url();?>assets/js/plugin/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

	<!-- jQuery Scrollbar -->
	<script src="<?=base_url();?>assets/js/plugin/jquery-scrollbar/jquery.scrollbar.min.js"></script>


	<!-- Chart JS -->
	<script src="<?=base_url();?>assets/js/plugin/chart.js/chart.min.js"></script>

	<!-- jQuery Sparkline -->
	<script src="<?=base_url();?>assets/js/plugin/jquery.sparkline/jquery.sparkline.min.js"></script>

	<!-- Chart Circle -->
	<script src="<?=base_url();?>assets/js/plugin/chart-circle/circles.min.js"></script>

	<!-- Datatables -->
	<script src="<?=base_url();?>assets/js/plugin/datatables/datatables.min.js"></script>

	<!-- Bootstrap Notify -->
	<script src="<?=base_url();?>assets/js/plugin/bootstrap-notify/bootstrap-notify.min.js"></script>

	<!-- jQuery Vector Maps -->
	<script src="<?=base_url();?>assets/js/plugin/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?=base_url();?>assets/js/plugin/jqvmap/maps/jquery.vmap.world.js"></script>

	<!-- Sweet Alert 2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	
	<!-- Pace JS -->
	<script src="<?=base_url();?>assets/plugin/pace/pace.js"></script>
	<link rel="stylesheet" href="<?=base_url();?>assets/plugin/pace/themes/green/pace-theme-corner-indicator.css">

	<!-- Atlantis JS -->
	<script src="<?=base_url();?>assets/js/atlantis.min.js"></script>

</head>
<body>
	<div class="wrapper">
		<div class="col-md-12">
				<div class="row">
					<div class="col-md-12 bg-secondary-gradient">
						<div class="panel-header">
							<div class="page-inner py-5">
								<div class="row">
									<div class="col-md-2">
									</div>
									<div class="col-md-2"  align="center">
										<img style="margin-top: -30px;" src="<?=base_url();?>assets/img/<?=getProfilCBT('logo_profil');?>" width="128" height="auto" />
									</div>
									<div class="col-md-6"  align="center">
										<h2 class="text-white pb-2 fw-bold" align="center">Admin Panel <?=getProfilCBT('title_profil');?></h2>
										<h5 class="text-white op-7 mb-2"><?=getProfilCBT('alamat_profil');?></h5>
									</div>
									<div class="col-md-2">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-inner mt--5">
					<div class="row mt--2">
						<div class="col-md-4"></div>
						<div class="col-md-4">
							<div class="card card-profile">
								<div class="card-header bg-secondary-gradient">
									<div class="profile-picture">
										<div class="avatar avatar-xl">
											<img src="<?=base_url();?>assets/img/profile.png" alt="..." class="avatar-img rounded-circle">
										</div>
									</div>
								</div>
								<div class="card-body">
									<div class="user-profile text-center" style="padding:0;margin:0;">
										<div class="name">Login</div>
										<form method="post" action="<?=base_url();?>LoginAdmin/Auth">
										<div class="form-group">
											<div class="input-icon">
												<span class="input-icon-addon">
													<i class="fa fa-user"></i>
												</span>
												<input type="text" class="form-control" placeholder="Username" name="username" />
											</div>
										</div>
										<div class="form-group">
											<div class="input-icon">
												<span class="input-icon-addon">
													<i class="fa fa-lock"></i>
												</span>
												<input type="password" class="form-control" placeholder="Password" name="password" />
											</div>
										</div>
										<div class="card-detail">
											<button type="submit" class="btn btn-secondary btn-rounded btn-block">Masuk</button>
										</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4"></div>
					</div>
				</div>
		</div>
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12 bg-secondary-gradient" style="height: auto;">
				<div class="panel-header">
					<div class="page-inner py-3">
						<div class="row">
							<div class="col-md-12 text-white text-center">
								Copyright &copy; Pusat Teknologi Informasi dan Pangkalan Data UIN Sunan Gunung Djati Bandung
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	<?=$this->session->flashdata('notif');?>
	</script>
</body>
</html>
